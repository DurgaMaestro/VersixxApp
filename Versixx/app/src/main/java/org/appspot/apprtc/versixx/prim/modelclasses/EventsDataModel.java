package org.appspot.apprtc.versixx.prim.modelclasses;

import java.util.ArrayList;


public class EventsDataModel {



    private String headerTitle;
    private ArrayList<ParticipantsItemModel> allItemsInSection;


    public EventsDataModel() {

    }
    public EventsDataModel(String eventdetailid, String eventid, String eventtitle, String eventdate, String eventduration, String participantstype, String noofparticipants, ArrayList<ParticipantsItemModel> allItemsInSection)
    {
        this.eventdetailid = eventdetailid;
        this.eventid = eventid;
        this.eventtitle = eventtitle;
        this.eventtitle = eventtitle;
        this.eventdate = eventdate;
        this.eventduration = eventduration;
        this.participantstype = participantstype;
        this.noofparticipants = noofparticipants;
        this.allItemsInSection = allItemsInSection;


    }
    public EventsDataModel(String headerTitle, ArrayList<ParticipantsItemModel> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.allItemsInSection = allItemsInSection;
    }


    private String eventdetailid;

    private String viewscount;
    private String likescount;
    private String eventid;


    private String eventtitle;

    private String eventdate;

    private String eventduration;


    private String participantstype;

    private String noofparticipants;


    private String participantid;

    private String participantname;

    private String participantemailid;

    private String acceptedstatus;

    private String changerequestmessage;
    public String getviewscount() {
        return viewscount;
    }

    public void setviewscount(String viewscount) {
        this.viewscount = viewscount;
    }

    public String getlikescount() {
        return likescount;
    }

    public void setlikescount(String likescount) {
        this.likescount = likescount;
    }


    public String getEventdetailid() {
        return eventdetailid;
    }

    public void setEventdetailid(String eventdetailid) {
        this.eventdetailid = eventdetailid;
    }

    public String getEventid() {
        return eventid;
    }

    public void setEventid(String eventid) {
        this.eventid = eventid;
    }

    public String getEventtitle() {
        return eventtitle;
    }

    public void setEventtitle(String eventtitle) {
        this.eventtitle = eventtitle;
    }

    public String getEventdate() {
        return eventdate;
    }

    public void setEventdate(String eventdate) {
        this.eventdate = eventdate;
    }

    public String getEventduration() {
        return eventduration;
    }

    public void setEventduration(String eventduration) {
        this.eventduration = eventduration;
    }

    public String getparticipantstype() {
        return participantstype;
    }

    public void setparticipantstype(String participantstype) {
        this.participantstype = participantstype;
    }

    public String getnoofparticipants() {
        return noofparticipants;
    }

    public void setnoofparticipants(String noofparticipants) {
        this.noofparticipants = noofparticipants;
    }

    public String getparticipantid() {
        return participantid;
    }

    public void setparticipantid(String participantid) {
        this.participantid = participantid;
    }

    public String getparticipantname() {
        return participantname;
    }

    public void setparticipantname(String participantname) {
        this.participantname = participantname;
    }

    public String getparticipantemailid() {
        return participantemailid;
    }

    public void setparticipantemailid(String participantemailid) {
        this.participantemailid = participantemailid;
    }
    public String getacceptedstatus() {
        return acceptedstatus;
    }

    public void setacceptedstatus(String acceptedstatus) {
        this.acceptedstatus = acceptedstatus;
    }
    public String getchangerequestmessage() {
        return changerequestmessage;
    }

    public void setchangerequestmessage(String changerequestmessage) {
        this.changerequestmessage = changerequestmessage;
    }
    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<ParticipantsItemModel> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<ParticipantsItemModel> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }


}
