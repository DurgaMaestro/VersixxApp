package org.appspot.apprtc.versixx.prim.Fragment.Friends;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.wang.avi.AVLoadingIndicatorView;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.MainActivity;
import org.appspot.apprtc.versixx.prim.Adapter.FriendsSendRequestAdapeter;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.modelclasses.friendslist;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SendFriendRequest extends Fragment {

    FriendsSendRequestAdapeter availableopenEventsadapter;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.loading_bar)
    AVLoadingIndicatorView loading;
    @Bind(R.id.tvErrorView)
    TextView tvErrorView;
    int clearstatus;
    LinearLayoutManager layoutManager;
    int currentPageNumber = 1, totalPages;
    private int MAX_SCROLLING_LIMIT = 10;
    private boolean isAlreadyLoading;

    private SearchView mSearchView;
    String token;
    private String TAG = SendFriendRequest.class.getSimpleName();
    List<friendslist> Friendslist = new ArrayList<>();
    LinearLayout lo;
    ImageView navmenu, search,back;
    private Toolbar toolbar,toolbar1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.friendsearch, container, false);
        ((MainActivity) getActivity()).settitle("Peoples You May Know");
        ((MainActivity) getActivity()).setti();
//        View v = getActivity().getCurrentFocus();
//        if (v != null) {
//            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//        }
////        ((MainActivity) getActivity()).getSupportActionBar().hide();
        ButterKnife.bind(this, view);
        token = MyApplication.getInstance().getPrefManager().getUser().getEmail();
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        availableopenEventsadapter = new FriendsSendRequestAdapeter(getActivity(), Friendslist);
        recyclerView.setAdapter(availableopenEventsadapter);
        loading.setVisibility(View.GONE);
        scrollFunction();
        currentPageNumber = 1;
        MAX_SCROLLING_LIMIT = 10;
        isAlreadyLoading = false;
        callFriendsList(1);
        toolbar = (Toolbar) view.findViewById(R.id.toolbarr);
        toolbar1 = (Toolbar) view.findViewById(R.id.toolbar);
        navmenu = (ImageView) view.findViewById(R.id.navmenu);
        back = (ImageView) view.findViewById(R.id.img);
        search = (ImageView) view.findViewById(R.id.lo);
        lo = (LinearLayout) view.findViewById(R.id.df);
        mSearchView = (SearchView) view.findViewById(R.id.searchView1);
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                toolbar.setVisibility(View.GONE);
                toolbar1.setVisibility(View.GONE);
                ((MainActivity) getActivity()).getSupportActionBar().show();

            }
        });
        ((MainActivity) getActivity()).search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.setVisibility(View.VISIBLE);
                toolbar1.setVisibility(View.GONE);

                mSearchView.setQueryHint("Search");
                mSearchView.setEnabled(false);
                mSearchView.setFocusable(false);
                mSearchView.setIconified(false);
                mSearchView.setIconified(false);
//                mSearchView.clearFocus();
                mSearchView.onActionViewExpanded();
                View vv = getActivity().getCurrentFocus();
                if (vv != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(vv, 0);
                }
                ((MainActivity) getActivity()).getSupportActionBar().hide();

            }
        });
        navmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (((MainActivity) getActivity()).drawerLayout.isDrawerOpen(((MainActivity) getActivity()).layoutLeftDrawer)) {
                    ((MainActivity) getActivity()).getSupportActionBar().hide();
                    ((MainActivity) getActivity()). drawerLayout.closeDrawer(((MainActivity) getActivity()).layoutLeftDrawer);
                } else {
                    ((MainActivity) getActivity()).getSupportActionBar().show();
                    ((MainActivity) getActivity()).drawerLayout.openDrawer(((MainActivity) getActivity()).layoutLeftDrawer);
                }

            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toolbar.setVisibility(View.VISIBLE);
                toolbar1.setVisibility(View.GONE);

            }
        });
//        mSearchView = (SearchView) view.findViewById(R.id.searchView1);
//        mSearchView.setQueryHint("Search");
//        mSearchView.setEnabled(false);
//        mSearchView.setFocusable(false);
//        mSearchView.setIconified(false);
//        mSearchView.setIconified(false);
//        mSearchView.clearFocus();
//        mSearchView.onActionViewExpanded();
//        mSearchView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//                }
//            }
//        });
//        mSearchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
//
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                // TODO Auto-generated method stub
//
//
//            }
//        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                final List<friendslist> filteredModelList = filter(Friendslist, newText);
                availableopenEventsadapter.setFilter(filteredModelList);
                return false;
            }
        });
//        View vb = getActivity().getCurrentFocus();
//        if (vb != null) {
//            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(vb.getWindowToken(), 0);
//        }
        return view;
    }

    private List<friendslist> filter(List<friendslist> models, String query) {
        query = query.toLowerCase();

        final List<friendslist> filteredModelList = new ArrayList<>();
        for (friendslist model : models) {
            final String text = model.getfriendname().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
    private void showInputMethod(View view) {
        InputMethodManager imm = (InputMethodManager)getActivity(). getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, 0);
        }
    }
    private void scrollFunction() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int last_seen_position = layoutManager.findLastVisibleItemPosition();

                if (!isAlreadyLoading
                        && ((last_seen_position + 2) > MAX_SCROLLING_LIMIT)) {

                    isAlreadyLoading = true;
                    MAX_SCROLLING_LIMIT += 10;

                    if (recyclerView != null) {
                        if (totalPages > currentPageNumber) {
                            currentPageNumber = currentPageNumber + 1;
                            loading.setVisibility(View.VISIBLE);
                            callFriendsList(0);
                        }
                    }
                }
            }
        });
    }

    private void callFriendsList(final int clear) {
        isAlreadyLoading = false;
        loading.setVisibility(View.GONE);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.friendssuggestion, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {


                        JSONArray json_mar = obj.getJSONArray("suggestionlist");
                        totalPages = obj.getInt("totalpages");

                        if (clear == 1) {
                            Friendslist.clear();
                        }
                        System.err.println("++++++++++" + json_mar.length());
                        if (json_mar.length() > 0) {


                            for (int i = 0; i < json_mar.length(); i++) {
                                JSONObject jsob = json_mar.getJSONObject(i);
                                friendslist eventlist = new friendslist(String.valueOf(jsob.getInt("userid")), jsob.getString("profileimage"),
                                        jsob.getString("name"), jsob.getString("emailid"), jsob.getString("livesin"));
                                Friendslist.add(eventlist);
                            }

                        }

                        availableopenEventsadapter.notifyDataSetChanged();

                    } else if (obj.getInt("status") == 0) {
                        tvErrorView.setVisibility(View.VISIBLE);
                    } else if (obj.getInt("status") == -1) {
                        MyApplication.getInstance().logout();
                    }

                } catch (JSONException e) {

                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getActivity().getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("token", token);
                params.put("searchkey", "");
                params.put("pagenumber", String.valueOf(currentPageNumber));
//                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);


    }


}
