package org.appspot.apprtc.versixx.prim.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.modelclasses.friendslist;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FriendsAdapeter extends RecyclerView.Adapter<FriendsAdapeter.ViewHolder>
{
    EditText message;
    String titlee;
    Context context;
    List<friendslist> list;
    String token;

    String eventdetailid,eventid;
    int priorityy=0;

    String alertMessage;
    int notifypostion;
    private String TAG = FriendsAdapeter.class.getSimpleName();
    public FriendsAdapeter(Context context, List<friendslist> list)
    {
        this.context = context;
        this.list = new ArrayList<>();
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {

        View view = LayoutInflater.from(context).inflate(R.layout.friendslist,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {

        token= MyApplication.getInstance().getPrefManager().getUser().getEmail();

        holder.tvTitle.setText(list.get(position).getfriendname());
        holder.duration.setText(list.get(position).getfriendlivesin());


        Glide.with(context).load(EndPoints.IMAGE_BASE_URL+""+list.get(position).getfriendprofileimage()).into(holder.image);


        holder.Unfriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                notifypostion = position;
                alertMessage = "Are you sure to Unfriend?";
                priorityy = 1;
                new AlertDialog.Builder(context)
                        .setTitle(alertMessage)
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {


                                callAcceptService(position);

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .show();

            }
        });
    }


    private void callAcceptService(final int position) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.unfriend, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {

                        Toast.makeText(context, "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                        list.remove(position);
                        notifyItemRemoved(position);
                        notifyDataSetChanged();


                    } else {
                        Toast.makeText(context, "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(context, "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(context, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                params.put("friendid", list.get(position).getfriendid());
                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);


    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;
        TextView duration,tvTitle;
        Button Unfriend;


        public ViewHolder(View itemView)
        {
            super(itemView);

            image = (ImageView)itemView.findViewById(R.id.profileimage);
            duration = (TextView)itemView.findViewById(R.id.tvDuration);
            tvTitle = (TextView)itemView.findViewById(R.id.tvTitle);
            Unfriend = (Button)itemView.findViewById(R.id.accept);
        }
    }
}
