package org.appspot.apprtc.versixx.prim.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.bumptech.glide.Glide;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.modelclasses.ViewEventModel;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewViewEventsAdapter extends RecyclerView.Adapter<NewViewEventsAdapter.SingleItemRowHolder> {

    private ArrayList<ViewEventModel> itemsList;
    private Context mContext;
    String token;

    private String TAG = NewViewEventsAdapter.class.getSimpleName();

    public NewViewEventsAdapter(Context context, ArrayList<ViewEventModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.viewadapter, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(final SingleItemRowHolder holder, final int i) {

        final ViewEventModel singleItem = itemsList.get(i);
        token = MyApplication.getInstance().getPrefManager().getUser().getEmail();
        holder.tvTitle.setText(singleItem.getparticipantname());
        holder.likes.setText(singleItem.getVotescount());

        Glide.with(mContext)
                .load(EndPoints.IMAGE_BASE_URL + singleItem.getparticipantprofileimage())

                .centerCrop()
                .into(holder.itemImage);
        if (itemsList.get(i).getDisplayvotebutton().equals("1")) {
            holder.vote.setVisibility(View.VISIBLE);
        } else {
            holder.vote.setVisibility(View.GONE);
        }
        if (itemsList.get(i).getDisplayrevertbutton().equals("1")) {
            holder.revote.setVisibility(View.VISIBLE);
        } else {
            holder.revote.setVisibility(View.GONE);
        }
        holder.vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callvote(itemsList.get(i).getparticipantid(), itemsList.get(i).geteventid());
                for(int ii=0;ii<itemsList.size();ii++)
                {
                    itemsList.get(ii).setDisplayvotebutton("0");
                    holder.revote.setVisibility(View.VISIBLE);
                }
                itemsList.get(i).setDisplayrevertbutton("1");
                if (itemsList.get(i).getDisplayvotebutton().equals("1")) {
                    holder.vote.setVisibility(View.VISIBLE);
                } else {
                    holder.vote.setVisibility(View.GONE);
                }
                if (itemsList.get(i).getDisplayrevertbutton().equals("1")) {
                    holder.revote.setVisibility(View.VISIBLE);
                } else {
                    holder.revote.setVisibility(View.GONE);
                }
                holder.revote.setVisibility(View.VISIBLE);

                notifyDataSetChanged();
            }
        });
        holder.revote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callrevote(itemsList.get(i).getparticipantid(), itemsList.get(i).geteventid());
                for(int ii=0;ii<itemsList.size();ii++)
                {
                    itemsList.get(ii).setDisplayrevertbutton("0");
                    itemsList.get(ii).setDisplayvotebutton("1");

                }
                if (itemsList.get(i).getDisplayvotebutton().equals("1")) {
                    holder.vote.setVisibility(View.VISIBLE);
                } else {
                    holder.vote.setVisibility(View.GONE);
                }
                if (itemsList.get(i).getDisplayrevertbutton().equals("1")) {
                    holder.revote.setVisibility(View.VISIBLE);
                } else {
                    holder.revote.setVisibility(View.GONE);
                }

                holder.revote.setVisibility(View.GONE);
                notifyDataSetChanged();
            }
        });
    }
    public ArrayList<ViewEventModel> getBox() {
        ArrayList<ViewEventModel> box = new ArrayList<ViewEventModel>();
        for (ViewEventModel p : itemsList) {
            if (p.isSelected())
                box.add(p);
        }
        return box;
    }


    public List<ViewEventModel> getList() {
        return this.itemsList;
    }
    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle, vote, revote, likes;

        protected ImageView itemImage;


        public SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
            this.vote = (TextView) view.findViewById(R.id.tvVotenow1);
            this.revote = (TextView) view.findViewById(R.id.tvRevert);
            this.likes = (TextView) view.findViewById(R.id.tvLike123);


//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
//                    Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();
//
//                }
//            });


        }

    }

    private void callrevote(final String participantid, final String eventDtailid) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.reverteventvote, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {

                        Toast.makeText(mContext, "" + obj.getString("msg"), Toast.LENGTH_LONG).show();


                    } else {
                        Toast.makeText(mContext, "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(mContext, "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(mContext, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                params.put("eventdetailid", eventDtailid);
                params.put("participantid", participantid);
//                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);


    }

    private void callvote(final String participantid, final String eventDtailid) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.posteventvote, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {

                        Toast.makeText(mContext, "" + obj.getString("msg"), Toast.LENGTH_LONG).show();


//                        notifyDataSetChanged();
//                        myawaitingevents(eventDtailid);

                    } else {
                        Toast.makeText(mContext, "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(mContext, "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(mContext, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                params.put("eventdetailid", eventDtailid);
                params.put("participantid", participantid);
//                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);


    }
}