package org.appspot.apprtc.versixx.prim.modelclasses;


public class navResponse
{

    String name;
    int image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public navResponse(String name, int image)
    {
        this.name = name;
        this.image = image;


    }
    public navResponse(String name)
    {
        this.name = name;

    }
    public navResponse()
    {

    }
}
