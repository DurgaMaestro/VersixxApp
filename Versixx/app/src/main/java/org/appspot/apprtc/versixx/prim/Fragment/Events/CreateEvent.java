package org.appspot.apprtc.versixx.prim.Fragment.Events;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.MainActivity;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.modelclasses.SearchUserResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class CreateEvent extends Fragment {
    ImageView more;
    List<String> autocompleteList;
    List<String> participantslist;
    List<String> autocompleteListid;
    List<String> autocompleteListid1;
    List<String> autocompleteListid2;
    EditText Title, Description;
    SeekBar seekBar;
    String seekbarprogrees;
    private String TAG = CreateEvent.class.getSimpleName();
    protected static TextView datepick, timepick;
    List<String> langlist = new ArrayList<String>();
    List<String> viewers = new ArrayList<String>();
    List<String> parttypes = new ArrayList<String>();
    List<String> partno = new ArrayList<String>();
    List<String> eventsobjid = new ArrayList<String>();
    List<String> eventsobjlist = new ArrayList<String>();
    List<String> langid = new ArrayList<String>();
    List<String> subcatid = new ArrayList<String>();
    List<String> subcat = new ArrayList<String>();
    Spinner Viewers, Language, EventCategory, EventSubCategory, ParticipantsType, NoofParticipants;
    String langgid;
    Button submit;
    LinearLayout pttype;
    TextView participant3;
    private ArrayAdapter<String> adapterr;
    AutoCompleteTextView autoCompleteParticpant2;
    AutoCompleteTextView autoCompleteParticpant3;
    AutoCompleteTextView autoCompleteParticpant1;
    String id, id1, id2;
    static String datetime;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.createevent, container, false);
        ((MainActivity) getActivity()).settitle("CreateEvent");
        autocompleteList = new ArrayList<>();

        autocompleteListid = new ArrayList<>();
        autocompleteListid1 = new ArrayList<>();
        autocompleteListid2 = new ArrayList<>();
        more = (ImageView) view.findViewById(R.id.more);
        participant3 = (TextView) view.findViewById(R.id.p3);
        Viewers = (Spinner) view.findViewById(R.id.viewers);
        Language = (Spinner) view.findViewById(R.id.lang);
        EventCategory = (Spinner) view.findViewById(R.id.event);
        EventSubCategory = (Spinner) view.findViewById(R.id.subevent);
        ParticipantsType = (Spinner) view.findViewById(R.id.parttype);
        NoofParticipants = (Spinner) view.findViewById(R.id.partno);
        Title = (EditText) view.findViewById(R.id.title);
        Description = (EditText) view.findViewById(R.id.des);
        submit = (Button) view.findViewById(R.id.submit);
        autoCompleteParticpant1 = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteParticpant1);
        autoCompleteParticpant2 = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteParticpant2);
        autoCompleteParticpant3 = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteParticpant3);
//        autoCompleteParticpant1.setText(MyApplication.getInstance().getPrefManager().getUser().getId());
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).Menus(v);
            }
        });
        getOffset();
        TimeZone tz = TimeZone.getDefault();
        Calendar cal = Calendar.getInstance();
        TimeZone tzz = cal.getTimeZone();
        Calendar call = Calendar.getInstance(Locale.getDefault());
        String
                timezoneStr = new SimpleDateFormat("Z").format(call.getTime());

        System.out.println(timezoneStr + " TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID() + "    " + tzz + "   " + Calendar.getInstance().getTimeZone().getDisplayName());
        System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID() + "    " + tzz + "   " + Calendar.getInstance().getTimeZone().getDisplayName());
        pttype = (LinearLayout) view.findViewById(R.id.visiblity);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((Title.getText().toString().equals(""))) {
                    Title.setError("Title is empty");
                } else if ((Description.getText().toString().equals(""))) {
                    Description.setError("Description is empty");
                } else if ((datepick.getText().toString().equals(""))) {
//                    datepick.setFocusable(true);
                    Toast.makeText(getActivity(), "" + "Please Fill Event Date", Toast.LENGTH_LONG).show();
//                    datepick.setError("Date is empty");
                } else if ((timepick.getText().toString().equals(""))) {
                    Toast.makeText(getActivity(), "" + "Please Fill Duration", Toast.LENGTH_LONG).show();
//                    timepick.setFocusable(true);
//                    timepick.setError("Time is empty");
                }

//                if ((Viewers.getSelectedItem().toString().equals("")) || (ParticipantsType.getSelectedItem().toString().equals("")) || (NoofParticipants.getSelectedItem().toString().equals("")) || (EventCategory.getSelectedItem().toString().equals(""))||Title.getText().toString().equals("")||Description.getText().toString().equals("")) {
//                    Toast.makeText(getActivity(), "" + "Please Fill all the fields", Toast.LENGTH_LONG).show();
//                }
                else {

                    buttonSubmit();

                }
            }
        });
        datepick = (TextView) view.findViewById(R.id.date);
        datepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerFragment mDatePicker = new DatePickerFragment();
                mDatePicker.show(getActivity().getFragmentManager(), "Select date");
            }
        });
        timepick = (TextView) view.findViewById(R.id.timee);
        timepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                durate();
//                TimePicker mTimePicker = new TimePicker();
//                mTimePicker.show(getActivity().getFragmentManager(), "Select time");

            }
        });
//        viewers.add("Select One");
        viewers.add("Friends");
        viewers.add("Friends of Friends");
        viewers.add("Public");
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getActivity(), R.layout.spinner, viewers);
        adapter1.setDropDownViewResource(R.layout.spinnerdropdowm);
        Viewers.setAdapter(adapter1);
        Viewers.setSelection(0);
//        parttypes.add("Select One");
        parttypes.add("Invite Participants");
        parttypes.add("Open");
        ArrayAdapter<String> adapter11 = new ArrayAdapter<String>(getActivity(), R.layout.spinner, parttypes);
        adapter1.setDropDownViewResource(R.layout.spinnerdropdowm);
        ParticipantsType.setAdapter(adapter11);
        ParticipantsType.setSelection(0);
//        partno.add("Select One");
        partno.add("2");
        partno.add("3");
        ArrayAdapter<String> adapter111 = new ArrayAdapter<String>(getActivity(), R.layout.spinner, partno);
        adapter1.setDropDownViewResource(R.layout.spinnerdropdowm);
        NoofParticipants.setAdapter(adapter111);
        NoofParticipants.setSelection(1);
        ArrayAdapter<String> adapter1111 = new ArrayAdapter<String>(getActivity(), R.layout.spinner, subcat);
        adapter1.setDropDownViewResource(R.layout.spinnerdropdowm);
        EventSubCategory.setAdapter(adapter1111);
        EventSubCategory.setSelection(0);
        getmyprofile();
        fetchevents();

        callAutocompleteFunctionality();
        ParticipantsType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (ParticipantsType.getSelectedItemPosition() == 0) {
                    pttype.setVisibility(View.VISIBLE);
                } else {
                    pttype.setVisibility(View.GONE);
                }
                autocompleteListid.clear();
                autoCompleteParticpant1.setText(MyApplication.getInstance().getPrefManager().getUser().getId());

                callSearchService(MyApplication.getInstance().getPrefManager().getUser().getId(), autoCompleteParticpant1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
        NoofParticipants.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (NoofParticipants.getSelectedItemPosition() == 0) {
                    participant3.setVisibility(View.GONE);
                    autoCompleteParticpant3.setVisibility(View.GONE);
                } else {
                    participant3.setVisibility(View.VISIBLE);

                    autoCompleteParticpant3.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
        return view;
    }


    public void durate() {

        final TextView stxtview;
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.seekbar);
        dialog.show();
        stxtview = (TextView) dialog.findViewById(R.id.tvvalue);
        seekBar = (SeekBar) dialog.findViewById(R.id.sbvalue);
        Button done = (Button) dialog.findViewById(R.id.button3);
        seekBar.setMax(0);
        seekBar.setMax(59);
        seekBar.setProgress(0);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                stxtview.setText("00 : " + progress);
                seekbarprogrees = "" + progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(seekbarprogrees!=null)
                {
                    timepick.setText("00:" + seekbarprogrees);
                }
                else {
                    timepick.setText("00:" + "00");
                }
            }
        });


    }

    public int getOffset() {
        TimeZone timezone = TimeZone.getDefault();
        TimeZone timeZone = TimeZone.getTimeZone("America/Los_Angeles");
        int seconds = timezone.getOffset(Calendar.ZONE_OFFSET) / 1000;
        int secondss = timeZone.getOffset(Calendar.ZONE_OFFSET) / 1000;
        double minutes = -seconds / 60;
        double minutess = -secondss / 60;
        double hours = minutes / 60;
        Double d = new Double(minutes);
        int i = d.intValue();
        System.out.println(minutes + " " + minutess+i);
        return i;
    }

    private void callAutocompleteFunctionality() {

        autoCompleteParticpant1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                autocompleteListid.clear();
                callSearchService(autoCompleteParticpant1.getText().toString(), autoCompleteParticpant1);


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        autoCompleteParticpant2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                autocompleteListid1.clear();
                callSearchService(autoCompleteParticpant2.getText().toString(), autoCompleteParticpant2);


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        autoCompleteParticpant3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                autocompleteListid2.clear();
                callSearchService(autoCompleteParticpant3.getText().toString(), autoCompleteParticpant3);


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void callSearchService(final String message, final AutoCompleteTextView auto) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.searchuser, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {

                        autocompleteList.clear();

                        JSONArray json_mar = obj.getJSONArray("userslist");

                        System.err.println("++++++++++" + json_mar.length());
                        if (json_mar.length() > 0) {


                            for (int i = 0; i < json_mar.length(); i++) {
                                JSONObject jsob = json_mar.getJSONObject(i);

                                SearchUserResponse eventlist = new SearchUserResponse(jsob.getString("userid"), jsob.getString("firstname"),
                                        jsob.getString("lastname"), jsob.getString("emailid"), jsob.getString("contactnumber"), jsob.getString("profileimagepath"));
                                Log.e(TAG, "json parsing lastname: " + jsob.getString("lastname") + jsob.getString("userid"));
                                autocompleteList.add(jsob.getString("emailid"));
                                autocompleteListid.add(jsob.getString("userid"));
                                autocompleteListid1.add(jsob.getString("userid"));
                                autocompleteListid2.add(jsob.getString("userid"));

                            }

                        }
                    } else {
                        Toast.makeText(getActivity(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                    ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, autocompleteList);

                    auto.setAdapter(adapter);

                    auto.setThreshold(1);


                    auto.setAdapter(adapter);
                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String name = MyApplication.getInstance().getPrefManager().getUser().getEmail();
                params.put("token", name);
                params.put("searchkey", message);
                params.put("pagenumber", "1");
                Log.e(TAG, "devicetype: " + name + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void buttonSubmit() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.createevent, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        timepick.setText("");
                        datepick.setText("");
                        Title.setText("");
                        Description.setText("");
                        Toast.makeText(getActivity(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                        MyAwaitingEvents fragment = new MyAwaitingEvents();

                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();

                    } else {
//                        timepick.setText("");
//                        datepick.setText("");
//                        Title.setText("");
//                        Description.setText("");

                        Toast.makeText(getActivity(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
//
//                        MyAwaitingEvents fragment = new MyAwaitingEvents();
//
//                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                        fragmentManager.beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String name = MyApplication.getInstance().getPrefManager().getUser().getEmail();
                int spinnerValue1 = Language.getSelectedItemPosition();
                System.out.println(spinnerValue1 + "one");
                TimeZone tz = TimeZone.getDefault();
                Calendar cal = Calendar.getInstance();
                TimeZone tzz = cal.getTimeZone();
                System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID() + "    " + tzz);
                int id = Integer.parseInt(langid.get(spinnerValue1));
                int spinnerValue11 = EventCategory.getSelectedItemPosition();
                System.out.println(spinnerValue1 + "one");
                int idd = Integer.parseInt(eventsobjid.get(spinnerValue11));
                int spinnerValue111 = EventSubCategory.getSelectedItemPosition();
                System.out.println(spinnerValue1 + "one");
                int idd1 = Integer.parseInt(subcatid.get(spinnerValue111));
                params.put("token", name);
                params.put("title", Title.getText().toString());
                if (Viewers.getSelectedItemPosition() == 0) {
                    params.put("viewers", String.valueOf("1"));
                } else if (Viewers.getSelectedItemPosition() == 1) {
                    params.put("viewers", String.valueOf("2"));
                } else if (Viewers.getSelectedItemPosition() == 2) {
                    params.put("viewers", String.valueOf("3"));
                }
//                params.put("viewers", String.valueOf(Viewers.getSelectedItemPosition()));
                params.put("language", String.valueOf(id));
                params.put("eventcategory", String.valueOf(idd));
                params.put("eventsubcategory", String.valueOf(idd1));
                params.put("date", datepick.getText().toString().trim());
                params.put("duration", timepick.getText().toString());
                if (ParticipantsType.getSelectedItemPosition() == 0) {
                    params.put("participantstype", String.valueOf("1"));
                } else if (ParticipantsType.getSelectedItemPosition() == 1) {
                    params.put("participantstype", String.valueOf("2"));
                }

//                params.put("participantstype", String.valueOf(ParticipantsType.getSelectedItemPosition()));
                params.put("noofparticipants", String.valueOf(NoofParticipants.getSelectedItem().toString()));
                params.put("description", Description.getText().toString());
                participantslist = new ArrayList<>();
                if (NoofParticipants.getSelectedItem().toString().equals("2")) {
                    if (autocompleteListid != null && autocompleteListid1 != null) {
                        participantslist.add(autocompleteListid.get(0).toString());
                        participantslist.add(autocompleteListid1.get(0).toString());
                    }
                } else if (NoofParticipants.getSelectedItem().toString().equals("3")) {
                    if (autocompleteListid != null && autocompleteListid1 != null && autocompleteListid2 != null) {
                        participantslist.add(autocompleteListid1.get(0).toString());
                        participantslist.add(autocompleteListid2.get(0).toString());
                        participantslist.add(autocompleteListid.get(0).toString());
                    }
                }
                JSONArray jsArray = new JSONArray(participantslist);
                params.put("participants", String.valueOf(jsArray));

//                String test = String.valueOf(jsonArray);
//                String firstWords = test.substring(0, test.lastIndexOf(" "));
//                String lastWord = test.substring(test.lastIndexOf(" ") + 1);


//                params.put("participants", "["+autocompleteListid.get(0).toString()+autocompleteListid1.get(0).toString()+autocompleteListid2.get(0).toString()+"]");

//                int i = 0;
//                String s = "";
//                Map<String, String> postParam = new HashMap<>();
////                int i=0;
//                for (String object : participantslist) {
//                    postParam.put("participants[" + (i++) + "]", object);
//                    // you first send both data with same param name as friendnr[] ....  now send with params friendnr[0],friendnr[1] ..and so on
//                }
//                for (String object : participantslist) {
//                    s += object;
//                    params.put("participants", "[" + object + "]");
//                    // you first send both data with same param name as friendnr[] ....  now send with params friendnr[0],friendnr[1] ..and so on
//                }
// params.put("participants", autocompleteListid1.get(0).toString());
//                params.put("participants", autocompleteListid2.get(0).toString());
                params.put("timezonename", tz.getID());
                params.put("timedifference", String.valueOf(getOffset()));


                Log.e(TAG, "devicetype: " +jsArray+ name + params.toString());
                return params;
            }
        };


        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private static String utilTime(int value) {

        if (value < 10)
            return "0" + String.valueOf(value);
        else
            return String.valueOf(value);
    }

    // Used to convert 24hr format to 12hr format with AM/PM values
    private static void updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        String hour = "";
        if (hours < 10)
            hour = "0" + hours;
        else
            hour = String.valueOf(hours);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hour).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

        datepick.setText(datetime + " " + aTime);

//        timepick.setText(aTime);
    }

    private void getmyprofile() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.getmyprofile, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        JSONObject profiledatas = obj.getJSONObject("profile");
                        String fnamee = profiledatas.getString("firstname");
                        String mobile = profiledatas.getString("contactnumber");
                        String lname = profiledatas.getString("lastname");
                        langgid = String.valueOf(profiledatas.getInt("languageid"));
                        fetchlang();

                    } else {
                        Toast.makeText(getActivity(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String name = MyApplication.getInstance().getPrefManager().getUser().getEmail();
                params.put("token", name);


                Log.e(TAG, "devicetype: " + name + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void getsubcat(final String id) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.listalleventsubcategory, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        JSONArray eventsArray = obj.getJSONArray("eventsubcategorylist");
                        subcat.clear();
                        subcatid.clear();
                        for (int i = 0; i < eventsArray.length(); i++) {
                            JSONObject eventsobj = (JSONObject) eventsArray.get(i);
                            Log.e(TAG, "json parsing error: " + eventsobj.getString("name"));

                            subcat.add(eventsobj.getString("name"));
                            subcatid.add(eventsobj.getString("id"));

                        }
                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getActivity(), R.layout.spinner, subcat);
                        adapter1.setDropDownViewResource(R.layout.spinnerdropdowm);
                        EventSubCategory.setAdapter(adapter1);
                        EventSubCategory.setSelection(0);

                    } else {
                        Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String name = MyApplication.getInstance().getPrefManager().getUser().getEmail();
                params.put("categoryid", id);


                Log.e(TAG, "devicetype: " + name + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void fetchlang() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.lang, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        JSONArray langArray = obj.getJSONArray("languagelist");
                        for (int i = 0; i < langArray.length(); i++) {
                            JSONObject langs = (JSONObject) langArray.get(i);
                            langlist.add(langs.getString("name"));
                            langid.add(langs.getString("id"));
//                            Log.e(TAG, "json parsing error: " + langs.getString("id"));

                        }
                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getActivity(), R.layout.spinner, langlist);
                        adapter1.setDropDownViewResource(R.layout.spinnerdropdowm);
                        Language.setAdapter(adapter1);
//                        int spinnerPosition = adapter1.getPosition("");
//                        int spinnerValue11 = countryitems.getSelectedItemPosition();
//                        int idcountry = Integer.parseInt(countryid.get(spinnerValue11));

//                        int drop_fk = Integer.parseInt(langid.get(Integer.parseInt(langgid)));
//                        languagespinner.setSelection(drop_fk);
                        if (langid.contains((langgid))) {
                            int position = langid.indexOf(langgid);

                            String drop_fk = (langlist.get(position));
                            Log.e(TAG, "json position: " + position + " " + drop_fk);
                            Language.setSelection((langlist.indexOf(drop_fk)));
//                            if (!drop_fk.equals(null)) {
//                                int spinnerPositions = adapter1.getPosition(drop_fk);
//                                languagespinner.setSelection(spinnerPositions);
//                            }
                        }
//                        for(int i=0; i < langid.size(); i++) {
//                            String s =(langid.get(i));
//                            //search the string
//                            Log.e(TAG, "json parsing error: " + s+"  "+langgid+"  "+langid.indexOf(String.valueOf(langgid)));
//                            if(s.equals(langgid)) {
//                                Log.e(TAG, "json parsing errorss: " + langid.get(i));
//                                languagespinner.setSelection(i);
//                            }
//                        }
//                        for (int i = 0; i < langid.size(); i++)
//                        {
//                            Log.e(TAG, "jid: " + langlist.get(i)+"");
//                            if(langid.get(i).equals(drop_fk))
//                            {
//                                languagespinner.setSelection(i);
//                            }
//                        }
//                        Log.e(TAG, "json parsing error: " + getIndex1(languagespinner, drop_fk));
//                        for (int i = 0; i < languagespinner.getCount(); i++)
//                        {
//                            if(langid.get(i).equals(langgid))
//                            {
//                                Cursor value = (Cursor) languagespinner.getItemAtPosition(i);
//                                long id = value.getLong(value.getColumnIndex(langgid));
//                                if (id == langgid)
//                                {
//                                    languagespinner.setSelection(i);
//                                }
////                                languagespinner.setSelection(i);
//                            }


//                        }

//                        languagespinner.setSelection(langgid);
//                        Log.e(TAG, "json parsing error: 1" + getIndex(languagespinner, langgid));
//                        languagespinner.setSelection(drop_fk);
                    } else {
                        Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

//    private void fetchlang() {
//        StringRequest strReq = new StringRequest(Request.Method.POST,
//                EndPoints.lang, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);
//
//                try {
//                    JSONObject obj = new JSONObject(response);
//
//                    if (obj.getInt("status") == 1) {
//                        JSONArray langArray = obj.getJSONArray("languagelist");
//                        for (int i = 0; i < langArray.length(); i++) {
//                            JSONObject langs = (JSONObject) langArray.get(i);
//                            langlist.add(langs.getString("name"));
//                            langid.add(langs.getString("id"));
//                            Log.e(TAG, "json parsing error: " + langs.getString("id"));
//
//                        }
//                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getActivity(), R.layout.spinner, langlist);
//                        adapter1.setDropDownViewResource(R.layout.spinnerdropdowm);
//                        Language.setAdapter(adapter1);
////                        int drop_fk = Integer.parseInt(langid.get(langgid));
//                        if (langid.contains((langgid))) {
//                            int position = langid.indexOf(langgid);
//
//                            String drop_fk = (langlist.get(position));
//                            Log.e(TAG, "json position: " + position + " " + drop_fk);
//                            Language.setSelection((langlist.indexOf(drop_fk)));
//                        }
//                    } else {
//                        Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
//                    }
//
//                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        MyApplication.getInstance().addToRequestQueue(strReq);
//    }


    public static class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
            updateTime(hourOfDay, minute);
//            timepick.setText("" + String.valueOf(hourOfDay) + " : " + String.valueOf(minute));
        }
    }

    public static class DatePickerFragment extends DialogFragment implements android.app.DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            return new android.app.DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {


            datetime = "" + String.valueOf(year) + "-" + String.valueOf(month+1) + "-" + String.valueOf(day);
            Log.e("datetime", "response: " + datetime);
            TimePicker mTimePicker = new TimePicker();
            mTimePicker.show(getActivity().getFragmentManager(), "Select time");
//            datepick.setText("" + String.valueOf(year) + " - " + String.valueOf(month) + " - " + String.valueOf(day));
        }
    }

    private void fetchevents() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.listalleventcategory, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        JSONArray eventsArray = obj.getJSONArray("eventcategorylist");
                        for (int i = 0; i < eventsArray.length(); i++) {
                            JSONObject eventsobj = (JSONObject) eventsArray.get(i);
                            Log.e(TAG, "json parsing error: " + eventsobj.getString("name"));
                            eventsobjlist.add(eventsobj.getString("name"));
                            eventsobjid.add(eventsobj.getString("id"));

                        }
                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getActivity(), R.layout.spinner, eventsobjlist);
                        adapter1.setDropDownViewResource(R.layout.spinnerdropdowm);
                        EventCategory.setAdapter(adapter1);
//                        EventCategory.setSelection(0);
                        EventCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                subcat.clear();
                                subcatid.clear();
                                int spinnerValue11 = EventCategory.getSelectedItemPosition();

                                int idd = Integer.parseInt(eventsobjid.get(spinnerValue11));

                                getsubcat(String.valueOf(idd));
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        MyApplication.getInstance().addToRequestQueue(strReq);
    }
}
