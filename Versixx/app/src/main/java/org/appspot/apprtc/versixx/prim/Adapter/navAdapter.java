package org.appspot.apprtc.versixx.prim.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.modelclasses.navResponse;

import java.util.Vector;

public class navAdapter extends RecyclerView.Adapter<navAdapter.ViewHolder>
{

    Vector<navResponse> vector ;
    Context context;

    public navAdapter(Context context, Vector<navResponse> vector)
    {
        this.vector = new Vector<>();
        this.context = context;
        this.vector = vector;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.navlist,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {


        navResponse modelclass = vector.get(position);
        holder.navimage.setImageResource(modelclass.getImage());
        holder.name.setText(modelclass.getName());




    }

    @Override
    public int getItemCount() {
        return vector.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView navimage;
        TextView name;
        RelativeLayout nr1;

        public ViewHolder(final View itemView) {
            super(itemView);
            navimage = (ImageView)itemView.findViewById(R.id.navimage);
            name = (TextView)itemView.findViewById(R.id.nabvtitle);
            nr1 = (RelativeLayout) itemView.findViewById(R.id.nr1);

        }
    }
}
