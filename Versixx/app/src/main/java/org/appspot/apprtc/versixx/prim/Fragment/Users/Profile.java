package org.appspot.apprtc.versixx.prim.Fragment.Users;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.MainActivity;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Durga on 20-10-2016.
 */
public class Profile extends Fragment {
    ImageView more;
    String langgid;
    EditText firstname, lastname, mobilenumber;
    Spinner language;
    ImageView profileimage, captureimage;
    private String TAG = Profile.class.getSimpleName();
    private static final int REQUEST_CAMERA = 0, GALLERY_CODE = 1;
    String imageupload = "";
    List<String> langlist = new ArrayList<String>();
    List<String> langid = new ArrayList<String>();
    Spinner languagespinner;
    Button update;
    RelativeLayout bg;
    int languageid;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.activity_profile, container, false);
        ((MainActivity) getActivity()).settitle("Profile");
        bg = (RelativeLayout) view.findViewById(R.id.miht);
        more = (ImageView) view.findViewById(R.id.more);
        more = (ImageView) view.findViewById(R.id.more);
        profileimage = (ImageView) view.findViewById(R.id.profile);
        captureimage = (ImageView) view.findViewById(R.id.capture);
        update = (Button) view.findViewById(R.id.submit);
        firstname = (EditText) view.findViewById(R.id.etFirstName);
        lastname = (EditText) view.findViewById(R.id.etLastName);
        mobilenumber = (EditText) view.findViewById(R.id.etMobile);
        languagespinner = (Spinner) view.findViewById(R.id.langspinner);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).Menus(v);
            }
        });
        getmyprofile();

        captureimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImageOption();
//                checkCameraPermission();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(imageupload.equals(""))
//                {
//                    Toast.makeText(getActivity(), "" +"Please Pick or Capture an image", Toast.LENGTH_LONG).show();
//
//                }
//                else if(mobilenumber.getText().toString().equals(""))
//                {
//                    Toast.makeText(getActivity(), "" +"Please Enter Ur Mobile No", Toast.LENGTH_LONG).show();
//                }
//                else{
                updatemyprofile();
//                }

            }
        });
        return view;
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] b = baos.toByteArray();
        String tempVal = Base64.encodeToString(b, Base64.DEFAULT);
        return tempVal;
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    private void fetchlang() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.lang, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        JSONArray langArray = obj.getJSONArray("languagelist");
                        for (int i = 0; i < langArray.length(); i++) {
                            JSONObject langs = (JSONObject) langArray.get(i);
                            langlist.add(langs.getString("name"));
                            langid.add(langs.getString("id"));
//                            Log.e(TAG, "json parsing error: " + langs.getString("id"));

                        }
                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getActivity(), R.layout.spinner, langlist);
                        adapter1.setDropDownViewResource(R.layout.spinnerdropdowm);
                        languagespinner.setAdapter(adapter1);
//                        int spinnerPosition = adapter1.getPosition("");
//                        int spinnerValue11 = countryitems.getSelectedItemPosition();
//                        int idcountry = Integer.parseInt(countryid.get(spinnerValue11));

//                        int drop_fk = Integer.parseInt(langid.get(Integer.parseInt(langgid)));
//                        languagespinner.setSelection(drop_fk);
                        if (langid.contains((langgid))) {
                            int position = langid.indexOf(langgid);

                            String drop_fk = (langlist.get(position));
                            Log.e(TAG, "json position: " + position + " " + drop_fk);
                            languagespinner.setSelection((langlist.indexOf(drop_fk)));
//                            if (!drop_fk.equals(null)) {
//                                int spinnerPositions = adapter1.getPosition(drop_fk);
//                                languagespinner.setSelection(spinnerPositions);
//                            }
                        }
//                        for(int i=0; i < langid.size(); i++) {
//                            String s =(langid.get(i));
//                            //search the string
//                            Log.e(TAG, "json parsing error: " + s+"  "+langgid+"  "+langid.indexOf(String.valueOf(langgid)));
//                            if(s.equals(langgid)) {
//                                Log.e(TAG, "json parsing errorss: " + langid.get(i));
//                                languagespinner.setSelection(i);
//                            }
//                        }
//                        for (int i = 0; i < langid.size(); i++)
//                        {
//                            Log.e(TAG, "jid: " + langlist.get(i)+"");
//                            if(langid.get(i).equals(drop_fk))
//                            {
//                                languagespinner.setSelection(i);
//                            }
//                        }
//                        Log.e(TAG, "json parsing error: " + getIndex1(languagespinner, drop_fk));
//                        for (int i = 0; i < languagespinner.getCount(); i++)
//                        {
//                            if(langid.get(i).equals(langgid))
//                            {
//                                Cursor value = (Cursor) languagespinner.getItemAtPosition(i);
//                                long id = value.getLong(value.getColumnIndex(langgid));
//                                if (id == langgid)
//                                {
//                                    languagespinner.setSelection(i);
//                                }
////                                languagespinner.setSelection(i);
//                            }


//                        }

//                        languagespinner.setSelection(langgid);
//                        Log.e(TAG, "json parsing error: 1" + getIndex(languagespinner, langgid));
//                        languagespinner.setSelection(drop_fk);
                    } else {
                        Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private int getIndex(Spinner spinner, String myString) {

        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).equals(myString)) {
                index = i;
            }
        }
        return index;
    }

    private int getIndex1(Spinner spinner, int myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (Integer.parseInt(spinner.getItemAtPosition(i).toString()) == myString) {
                index = i;
                i = spinner.getCount();//will stop the loop, kind of break, by making condition false
            }
        }
        return index;
    }

    private void getmyprofile() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.getmyprofile, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        JSONObject profiledatas = obj.getJSONObject("profile");
                        String fnamee = profiledatas.getString("firstname");
                        String mobile = profiledatas.getString("contactnumber");
                        String lname = profiledatas.getString("lastname");
                        langgid = String.valueOf(profiledatas.getInt("languageid"));
                        fetchlang();
                        firstname.setText(fnamee);
                        lastname.setText(lname);
                        mobilenumber.setText(mobile);

                        Log.e(TAG, "json parsing error: " + profiledatas.getString("languageid") + profiledatas.getString("profileimage"));
//                        Glide.with(getActivity()).load(EndPoints.IMAGE_BASE_URL + profiledatas.getString("profileimage")).into(profileimage);
//                        Glide.with(this.getActivity())
//                                .load(R.drawable.your_image)
//                                .into(new LinearLayoutTarget((LinearLayout) yourLinearLayoutInstanceHere));
                        Glide.with(getActivity()).load(EndPoints.IMAGE_BASE_URL + profiledatas.getString("profileimage"))

                                .centerCrop()
                                .crossFade()
                                .into(profileimage);

                    } else {
                        Toast.makeText(getActivity(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String name = MyApplication.getInstance().getPrefManager().getUser().getEmail();
                params.put("token", name);


                Log.e(TAG, "devicetype: " + name + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    public void setimage(String a) {
        profileimage = (ImageView) view.findViewById(R.id.profile);

        Drawable drawable = new BitmapDrawable(getResources(), StringToBitMap(a));
        profileimage.setBackgroundDrawable(drawable);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (resultCode == Activity.RESULT_OK && requestCode == 0) {

//            profileimage = (ImageView) view.findViewById(R.id.profile);
            Bitmap picture = (Bitmap) data.getExtras().get("data");
            profileimage.setVisibility(View.GONE);
            RelativeLayout bg = (RelativeLayout) view.findViewById(R.id.miht);
            Drawable drawable = new BitmapDrawable(getResources(), picture);
            bg.setBackgroundDrawable(drawable);
//            Drawable drawable = new BitmapDrawable(getResources(), picture);
//            profileimage.setBackgroundDrawable(drawable);
//            profileimage.setImageBitmap(picture);
            imageupload = BitMapToString(picture);
//            setimage(imageupload);
//            BitmapDrawable ob = new BitmapDrawable(getActivity().getResources(), picture);
//
//            BitmapDrawable bitmapDrawable = new BitmapDrawable(picture);
//            profileimage.setBackgroundDrawable(ob);


        }
        if (requestCode == GALLERY_CODE && resultCode == Activity.RESULT_OK && null != data) {

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), data.getData());
                RelativeLayout bg = (RelativeLayout) view.findViewById(R.id.miht);
                profileimage.setVisibility(View.GONE);
                Drawable drawable = new BitmapDrawable(getResources(), bitmap);
                bg.setBackgroundDrawable(drawable);
                imageupload = BitMapToString(bitmap);
//                setimage(imageupload);

//            Bitmap picture = null;
//            try {
//                profileimage = (ImageView) view.findViewById(R.id.profile);
//                picture = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), data.getData());
//                Drawable drawable = new BitmapDrawable(getResources(), picture);
//                profileimage.setBackgroundDrawable(drawable);
//
//////            profileimage.setImageBitmap(picture);
//                imageupload = BitMapToString(picture);
////                profileimage.setBackground(getResources().getDrawable(picture));
////                BitmapDrawable ob = new BitmapDrawable(getActivity().getResources(), picture);
//
//                BitmapDrawable bitmapDrawable = new BitmapDrawable(picture);
//                profileimage.setBackgroundDrawable(ob);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void takePicture() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(intent, 0);
    }


    private void requestCameraPermission() {


        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
                REQUEST_CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA) {

            Log.i(TAG, "Received response for Camera permission request.");


            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                takePicture();

            } else {

                Toast.makeText(getActivity(), "You need to grant camera permission to use camera", Toast.LENGTH_LONG).show();
            }

        }
    }

    private void selectImageOption() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Capture Photo")) {
                    checkCameraPermission();

//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp1.jpg");
//                    mImageCaptureUri = Uri.fromFile(f);
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
//                    startActivityForResult(intent, CAMERA_CODE);

                } else if (items[item].equals("Choose from Gallery")) {

                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            GALLERY_CODE);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void updatemyprofile() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.updatemyprofile, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        Toast.makeText(getActivity(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                        Intent i = new Intent(getActivity(), MainActivity.class);
                        startActivity(i);


                    } else {
                        Toast.makeText(getActivity(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();

                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String name = MyApplication.getInstance().getPrefManager().getUser().getEmail();
                int spinnerValue1 = languagespinner.getSelectedItemPosition();
                System.out.println(spinnerValue1 + "one");
                int id = Integer.parseInt(langid.get(spinnerValue1));
                params.put("token", name);
                params.put("firstname", firstname.getText().toString());
                params.put("lastname", lastname.getText().toString());
                params.put("contactnumber", mobilenumber.getText().toString());
                params.put("languageid", String.valueOf(id));
                params.put("uploadprofileimage", imageupload);


                Log.e(TAG, "devicetype: " + imageupload + name + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    void checkCameraPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            requestCameraPermission();


        } else {

            takePicture();

        }
    }
}
