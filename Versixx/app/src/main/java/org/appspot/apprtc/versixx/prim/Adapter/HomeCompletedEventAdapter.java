package org.appspot.apprtc.versixx.prim.Adapter;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.modelclasses.EventsDataModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class HomeCompletedEventAdapter extends RecyclerView.Adapter<HomeCompletedEventAdapter.ItemRowHolder> {

    private ArrayList<EventsDataModel> dataList;
    private Context mContext;

    public HomeCompletedEventAdapter(Context context, ArrayList<EventsDataModel> dataList) {
        this.dataList = dataList;
        this.mContext = context;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {

        final String sectionName = dataList.get(i).getHeaderTitle();
        ArrayList singleSectionItems = dataList.get(i).getAllItemsInSection();
        itemRowHolder.itemTitle.setText(sectionName);
//        itemRowHolder.tvDate.setText(" "+dataList.get(i).getEventdate() + "  " );


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
        String dtStart = dataList.get(i).getEventdate();
        try {
            Date d = format.parse(dtStart);
            format.format(d.getTime()+ TimeZone.getDefault().getRawOffset());

            System.out.println(format.format(d.getTime() + TimeZone.getDefault().getRawOffset()) + " DateTime(date);"+dataList.get(i).getEventdate());
            itemRowHolder.tvDate.setText(format.format(d.getTime() + TimeZone.getDefault().getRawOffset()));

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        itemRowHolder.likes.setText(" "+dataList.get(i).getlikescount()+" Likes");
        itemRowHolder.views.setText(" "+dataList.get(i).getviewscount()+" Views");
        HomeEventListDataAdapter itemListDataAdapter = new HomeEventListDataAdapter(mContext, singleSectionItems);

        itemRowHolder.recycler_view_list.setHasFixedSize(true);
        itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);


         itemRowHolder.recycler_view_list.setNestedScrollingEnabled(false);

        itemRowHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Toast.makeText(v.getContext(), "click event on more, " + sectionName, Toast.LENGTH_SHORT).show();



            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;

        protected RecyclerView recycler_view_list;
        TextView tvDate, tvTitle, likes, views, t1, t2, t3;

        protected Button btnMore;



        public ItemRowHolder(View view) {
            super(view);
            this.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
            this.recycler_view_list = (RecyclerView) view.findViewById(R.id.recycler_view_list);
            this.btnMore= (Button) view.findViewById(R.id.btnMore);
            this.tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            this.views = (TextView) itemView.findViewById(R.id.tvViews);
            this.likes = (TextView) itemView.findViewById(R.id.tvLikes);

        }

    }

}