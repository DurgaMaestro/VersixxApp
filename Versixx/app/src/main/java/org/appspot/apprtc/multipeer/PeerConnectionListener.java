package org.appspot.apprtc.multipeer;

import org.webrtc.PeerConnection;

/**
 */

public interface PeerConnectionListener {
    public void peerConnectionCreated(BasePeerConnection peerConnection);
}
