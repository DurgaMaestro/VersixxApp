package org.appspot.apprtc.versixx.prim.modelclasses;



public class ParticipantssResponse
{
    String participantname;
    String participantemail;
    String acceptedstatus;
    String title;
    String changerequestmessage;

    String participantid;

    public String getParticipantid() {
        return participantid;
    }

    public void setParticipantid(String participantid) {
        this.participantid = participantid;
    }

    public String getChangerequestmessage() {
        return changerequestmessage;
    }

    public void setChangerequestmessage(String changerequestmessage) {
        this.changerequestmessage = changerequestmessage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParticipantname() {
        return participantname;
    }

    public void setParticipantname(String participantname) {
        this.participantname = participantname;
    }

    public String getParticipantemail() {
        return participantemail;
    }

    public void setParticipantemail(String participantemail) {
        this.participantemail = participantemail;
    }

    public String getAcceptedstatus() {
        return acceptedstatus;
    }

    public void setAcceptedstatus(String acceptedstatus) {
        this.acceptedstatus = acceptedstatus;
    }
}
