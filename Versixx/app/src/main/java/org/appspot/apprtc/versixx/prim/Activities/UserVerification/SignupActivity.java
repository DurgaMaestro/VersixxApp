package org.appspot.apprtc.versixx.prim.Activities.UserVerification;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SignupActivity extends AppCompatActivity {

    private String TAG = SignupActivity.class.getSimpleName();
    EditText Name, LastName, Email, Password, Cpwd;
    Button send;
    String MobilePattern = "[0-9]{10}";
    Pattern pattern;
    String naame;
    String mailid;
    String pwd;
    ImageView back;
    String cpwd;
    CheckBox st;
    String phone;
    Spinner countryitems, languagespinner;
    String emailRegEx = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";
    List<String> countrylist = new ArrayList<String>();
    List<String> countryid = new ArrayList<String>();
    List<String> langlist = new ArrayList<String>();
    List<String> langid = new ArrayList<String>();
    int drop_fk;
    TextView textmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        Name = (EditText) findViewById(R.id.name);
        LastName = (EditText) findViewById(R.id.mobile);
        Email = (EditText) findViewById(R.id.email);
        Password = (EditText) findViewById(R.id.pwd);
        countryitems = (Spinner) findViewById(R.id.countryspinner);
        languagespinner = (Spinner) findViewById(R.id.langspinner);
        Cpwd = (EditText) findViewById(R.id.conpwd);
        back = (ImageView) findViewById(R.id.img);
        textmsg = (TextView) findViewById(R.id.fbdb);
        Bundle bundle = getIntent().getExtras();
        String namefb = bundle.getString("name");
        String emailfb = bundle.getString("email");
        if (!namefb.equals("")) {
            Name.setText(namefb);
            Email.setText(emailfb);
            textmsg.setText("Hi " + namefb + ", Please fill following details to create login for Versixx");
        }
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });
        fetchcountry();
        fetchlang();
        send = (Button) findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                naame = Name.getText().toString();
                mailid = Email.getText().toString();
                pwd = Password.getText().toString();
                cpwd = Cpwd.getText().toString();
                phone = LastName.getText().toString();
                pattern = Pattern.compile(emailRegEx);
                Matcher matcher = pattern.matcher(mailid);
                if (naame.equals("") || mailid.equals("") || phone.equals("") || pwd.equals("") || cpwd.equals("")) {

                    Toast.makeText(getApplicationContext(), "Enter All Fields", Toast.LENGTH_LONG).show();
                    return;
                } else if (!pwd.equals(cpwd)) {
                    Toast.makeText(getApplicationContext(), "Password does not match", Toast.LENGTH_LONG).show();
                    Cpwd.requestFocus();
                    return;
                }
                else if (!matcher.find()) {
                    Toast.makeText(getApplicationContext(), "Please provide valid email Format", Toast.LENGTH_LONG).show();
                    Email.requestFocus();

                    return;
                } else if (pwd.length() < 5) {
                    Toast.makeText(getApplicationContext(), "You must enter 5 characters in your password", Toast.LENGTH_LONG).show();
                    Password.requestFocus();
                    return;
                } else {
                    signup();

                }

            }
        });
    }

    private void fetchlang() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.lang, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        JSONArray langArray = obj.getJSONArray("languagelist");
                        for (int i = 0; i < langArray.length(); i++) {
                            JSONObject langObj = (JSONObject) langArray.get(i);
                            langlist.add(langObj.getString("name"));
                            langid.add(langObj.getString("id"));
//                            Log.e(TAG, "json parsing error: " + chatRoomsObj.getString("id"));

                        }
                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(SignupActivity.this, R.layout.spinner, langlist);
                        adapter1.setDropDownViewResource(R.layout.spinnerdropdowm);
                        languagespinner.setAdapter(adapter1);
                        languagespinner.setSelection(0);
                    } else {
                        Toast.makeText(getApplicationContext(), "" +obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void fetchcountry() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.listallcountry, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        JSONArray countrylistArray = obj.getJSONArray("countrylist");
                        for (int i = 0; i < countrylistArray.length(); i++) {
                            JSONObject Obj = (JSONObject) countrylistArray.get(i);
                            countrylist.add(Obj.getString("name"));
                            countryid.add(Obj.getString("id"));
                            Log.e(TAG, "json parsing error: " + Obj.getString("id"));

                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignupActivity.this, R.layout.spinner, countrylist);
                        adapter.setDropDownViewResource(R.layout.spinnerdropdowm);
                        countryitems.setAdapter(adapter);
                        countryitems.setSelection(0);
                    } else {
                        Toast.makeText(getApplicationContext(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void signup() {

        final String name = Name.getText().toString();
        final String mobile = LastName.getText().toString();
        final String email = Email.getText().toString();
        final String password = Password.getText().toString();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.SignUp, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {

                        Toast.makeText(getApplicationContext(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();

                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));

                        finish();


                    } else {
                        Toast.makeText(getApplicationContext(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                int spinnerValue1 = languagespinner.getSelectedItemPosition();
                int spinnerValue11 = countryitems.getSelectedItemPosition();
//                System.out.println(spinnerValue1 + "one");
                int idcountry = Integer.parseInt(countryid.get(spinnerValue11));
                int idlang = Integer.parseInt(langid.get(spinnerValue1));
                params.put("firstname", name);
                params.put("countryid", String.valueOf(idcountry));
                params.put("emailid", email);
                params.put("password", password);
                params.put("lastname", mobile);
                params.put("languageid", String.valueOf(idlang));
//                Log.e(TAG, "devicetype: " + params.toString()+" "+idcountry+" "+idlang);
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }


}
