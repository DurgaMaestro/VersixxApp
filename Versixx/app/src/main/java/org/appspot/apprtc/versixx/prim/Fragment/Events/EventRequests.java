package org.appspot.apprtc.versixx.prim.Fragment.Events;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.wang.avi.AVLoadingIndicatorView;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.MainActivity;
import org.appspot.apprtc.versixx.prim.Adapter.Eventadapter;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.modelclasses.EventRequestlist;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EventRequests extends Fragment {
    private String TAG = EventRequests.class.getSimpleName();
    Eventadapter eventadapter;
    int clearstatus=0;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.loading_bar)
    AVLoadingIndicatorView loading;
    @Bind(R.id.tvErrorView)
    TextView tvErrorView;
    @Bind(R.id.openeve)
    LinearLayout openEvents;
    LinearLayoutManager layoutManager;
    int currentPageNumber = 1, totalPages;
    private int MAX_SCROLLING_LIMIT = 10;
    private boolean isAlreadyLoading;
    String token;
    List<EventRequestlist> eventrequestlistList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.genericrecyclerview, container, false);
        ((MainActivity) getActivity()).settitle("Event Requests");
        ButterKnife.bind(this, view);
        token = MyApplication.getInstance().getPrefManager().getUser().getEmail();
        eventrequestlistList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        eventadapter = new Eventadapter(getActivity(), eventrequestlistList, EventRequests.this);
        recyclerView.setAdapter(eventadapter);
        loading.setVisibility(View.GONE);
        scrollFunction();
        clearstatus = 1;
        currentPageNumber = 1;
        MAX_SCROLLING_LIMIT=10;
        isAlreadyLoading = false;
        callEventrequestList(1);


        return view;
    }


    private void scrollFunction() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int last_seen_position = layoutManager.findLastVisibleItemPosition();

                if (!isAlreadyLoading
                        && ((last_seen_position + 2) > MAX_SCROLLING_LIMIT)) {

                    isAlreadyLoading = true;
                    MAX_SCROLLING_LIMIT += 10;

                    if (recyclerView != null) {
                        if (totalPages > currentPageNumber) {
                            currentPageNumber = currentPageNumber + 1;
                            loading.setVisibility(View.VISIBLE);
                            clearstatus = 0;
                            callEventrequestList(0);
                        }
                    }
                }
            }
        });
    }

    private void callEventrequestList(final int clear) {
        isAlreadyLoading = false;
        loading.setVisibility(View.GONE);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.eventparticipaterequests, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        totalPages = obj.getInt("totalpages");
//                        currentPageNumber= obj.getInt("currentpageno");
                        JSONArray json_mar = obj.getJSONArray("eventrequestlist");
                        if (clear == 1) {
                            eventrequestlistList.clear();
                        }
//                        System.err.println("++++++++++" + json_mar.length());
                        if (json_mar.length() > 0) {
                            for (int i = 0; i < json_mar.length(); i++) {
                                JSONObject jsob = json_mar.getJSONObject(i);
                                EventRequestlist eventlist = new EventRequestlist(jsob.getString("eventrequestid"), jsob.getString("eventid"),
                                        jsob.getString("senderprofileimg"), jsob.getString("eventtitle"), jsob.getString("eventdate"), jsob.getString("eventduration"));
                                eventrequestlistList.add(eventlist);
                            }

                        }

                        eventadapter.notifyDataSetChanged();

                    } else if (obj.getInt("status") == 0) {
                        tvErrorView.setVisibility(View.VISIBLE);
                    } else if (obj.getInt("status") == -1) {
                        MyApplication.getInstance().logout();
                    }
                } catch (JSONException e) {

                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("token", token);
                params.put("searchkey", "");
                params.put("pagenumber", String.valueOf(currentPageNumber));
//                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            String vv = "" + data.getExtras().get("data");
//            Log.e("data", vv);
        }

    }
}
