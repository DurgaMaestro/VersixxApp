package org.appspot.apprtc.versixx.prim.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.MainActivity;

/**
 * Created by Durga on 20-10-2016.
 */
public class Battle extends Fragment
{
    ImageView more;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.home,container,false);
        more=(ImageView)view.findViewById(R.id.more);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).Menus(v);
            }
        });


        return view;
    }
}
