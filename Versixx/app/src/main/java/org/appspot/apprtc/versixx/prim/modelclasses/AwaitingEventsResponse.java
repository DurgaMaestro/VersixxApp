package org.appspot.apprtc.versixx.prim.modelclasses;

import java.util.List;


public class AwaitingEventsResponse {
    public AwaitingEventsResponse(){}

    public AwaitingEventsResponse(String eventdetailid, String eventid, String eventtitle, String eventdate, String eventduration, String participantstype, String noofparticipants, List<participantss> participantsss)
    {
        this.eventdetailid = eventdetailid;
        this.eventid = eventid;
        this.eventtitle = eventtitle;
        this.eventtitle = eventtitle;
        this.eventdate = eventdate;
        this.eventduration = eventduration;
        this.participantstype = participantstype;
        this.noofparticipants = noofparticipants;
        this.participantsss = participantsss;


    }
    private String eventdetailid;

    private String eventid;


    private String eventtitle;

    private String eventdate;

    private String eventduration;


    private String participantstype;

    private String noofparticipants;


    private String participantid;

    private String participantname;

    private String participantemailid;

    private String acceptedstatus;

    private String changerequestmessage;


    public String getEventdetailid() {
        return eventdetailid;
    }

    public void setEventdetailid(String eventdetailid) {
        this.eventdetailid = eventdetailid;
    }

    public String getEventid() {
        return eventid;
    }

    public void setEventid(String eventid) {
        this.eventid = eventid;
    }

    public String getEventtitle() {
        return eventtitle;
    }

    public void setEventtitle(String eventtitle) {
        this.eventtitle = eventtitle;
    }

    public String getEventdate() {
        return eventdate;
    }

    public void setEventdate(String eventdate) {
        this.eventdate = eventdate;
    }

    public String getEventduration() {
        return eventduration;
    }

    public void setEventduration(String eventduration) {
        this.eventduration = eventduration;
    }

    public String getparticipantstype() {
        return participantstype;
    }

    public void setparticipantstype(String participantstype) {
        this.participantstype = participantstype;
    }

    public String getnoofparticipants() {
        return noofparticipants;
    }

    public void setnoofparticipants(String noofparticipants) {
        this.noofparticipants = noofparticipants;
    }

    public String getparticipantid() {
        return participantid;
    }

    public void setparticipantid(String participantid) {
        this.participantid = participantid;
    }

    public String getparticipantname() {
        return participantname;
    }

    public void setparticipantname(String participantname) {
        this.participantname = participantname;
    }

    public String getparticipantemailid() {
        return participantemailid;
    }

    public void setparticipantemailid(String participantemailid) {
        this.participantemailid = participantemailid;
    }
    public String getacceptedstatus() {
        return acceptedstatus;
    }

    public void setacceptedstatus(String acceptedstatus) {
        this.acceptedstatus = acceptedstatus;
    }
    public String getchangerequestmessage() {
        return changerequestmessage;
    }

    public void setchangerequestmessage(String changerequestmessage) {
        this.changerequestmessage = changerequestmessage;
    }
    List<participantss> participantsss;
    public List<participantss> getChatlist() {
        return participantsss;
    }

    public void setChatlist(List<participantss> participantsss) {
        this.participantsss = participantsss;
    }

    public class participantss {
        public participantss(){}
        public participantss(String participantid, String participantname, String participantemailid, String acceptedstatus, String changerequestmessage, String eventid) {

            this.participantid = participantid;
            this.participantname = participantname;
            this.participantemailid = participantemailid;
            this.acceptedstatus = acceptedstatus;
            this.changerequestmessage = changerequestmessage;
            this.eventid = eventid;

        }

        private String eventid;

        private String participantid;

        private String participantname;

        private String participantemailid;

        private String acceptedstatus;

        private String changerequestmessage;

        public String geteventid() {
            return eventid;
        }

        public void seteventid(String eventid) {
            this.eventid = eventid;
        }

        public String getparticipantid() {
            return participantid;
        }

        public void setparticipantid(String participantid) {
            this.participantid = participantid;
        }

        public String getparticipantname() {
            return participantname;
        }

        public void setparticipantname(String participantname) {
            this.participantname = participantname;
        }

        public String getparticipantemailid() {
            return participantemailid;
        }

        public void setparticipantemailid(String participantemailid) {
            this.participantemailid = participantemailid;
        }

        public String getacceptedstatus() {
            return acceptedstatus;
        }

        public void setacceptedstatus(String acceptedstatus) {
            this.acceptedstatus = acceptedstatus;
        }

        public String getchangerequestmessage() {
            return changerequestmessage;
        }

        public void setchangerequestmessage(String changerequestmessage) {
            this.changerequestmessage = changerequestmessage;
        }
    }
}
