package org.appspot.apprtc.versixx.prim.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.modelclasses.CommentListPojo;

import java.util.ArrayList;
import java.util.List;

public class Commentadapter extends RecyclerView.Adapter<Commentadapter.ViewHolder>
{
    EditText message;
    String titlee;
    Context context;
    Fragment fragment;
    List<CommentListPojo> list;
    String token;
    private String TAG = Commentadapter.class.getSimpleName();
    String eventrequestid,eventid;
    int priorityy=0;
    String alertMessage;
    int notifypostion;

    public Commentadapter(Context context, List<CommentListPojo> list, Fragment eventRequest)
    {
        this.context = context;
        this.list = new ArrayList<>();
        this.list = list;
        this.fragment = eventRequest;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {

        View view = LayoutInflater.from(context).inflate(R.layout.comments,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {


        token= MyApplication.getInstance().getPrefManager().getUser().getEmail();
        holder.tvTitle.setText(list.get(position).getEventtitle());
        holder.tvTitlee.setText(list.get(position).getEventduration());
        holder.duration.setText(list.get(position).getEventdate());
        eventrequestid = list.get(position).getEventrequestid();
        eventid = list.get(position).getEventid();
        notifypostion = position;

    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;
        TextView duration,tvTitle,tvTitlee;
        Button accept,request,reject;

        public ViewHolder(View itemView)
        {
            super(itemView);

            image = (ImageView)itemView.findViewById(R.id.profileimage);
            duration = (TextView)itemView.findViewById(R.id.tvDuration);
            tvTitle = (TextView)itemView.findViewById(R.id.tvTitle);
            tvTitlee = (TextView)itemView.findViewById(R.id.tvTitlee);
            accept = (Button)itemView.findViewById(R.id.accept);
            request = (Button)itemView.findViewById(R.id.request);
            reject = (Button)itemView.findViewById(R.id.reject);
        }
    }
}
