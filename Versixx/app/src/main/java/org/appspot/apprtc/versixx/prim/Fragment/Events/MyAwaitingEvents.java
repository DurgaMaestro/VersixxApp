package org.appspot.apprtc.versixx.prim.Fragment.Events;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.wang.avi.AVLoadingIndicatorView;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.EventParticipantActivity;
import org.appspot.apprtc.versixx.prim.Activities.MainActivity;
import org.appspot.apprtc.versixx.prim.Adapter.MyAwaitingEventsadapter;
import org.appspot.apprtc.versixx.prim.app.ClickListener;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.app.RecyclerTouchListener;
import org.appspot.apprtc.versixx.prim.modelclasses.AwaitingEventsResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MyAwaitingEvents extends Fragment {
    MyAwaitingEventsadapter awaiteventadapter;

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.loading_bar)
    AVLoadingIndicatorView loading;
    @Bind(R.id.tvErrorView)
    TextView tvErrorView;
    LinearLayoutManager layoutManager;
    private String TAG = MyAwaitingEvents.class.getSimpleName();
    int currentPageNumber = 1, totalPages;
    private int MAX_SCROLLING_LIMIT = 10;
    private boolean isAlreadyLoading;
    String token;
    List<AwaitingEventsResponse> awaitingeventlist = new ArrayList<>();
    List<AwaitingEventsResponse.participantss> l = new ArrayList<AwaitingEventsResponse.participantss>();
    ArrayList<String> participantname;
    ArrayList<String> participantid;
    ArrayList<String> participantemailid;
    ArrayList<String> acceptedstatus;
    ArrayList<String> changerequestmessage;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.genericrecyclerview1, container, false);
        ((MainActivity) getActivity()).settitle("My Awaiting Events");
        ButterKnife.bind(this, view);
        token = MyApplication.getInstance().getPrefManager().getUser().getEmail();
        participantname = new ArrayList<>();
        participantid = new ArrayList<>();
        participantemailid = new ArrayList<>();
        acceptedstatus = new ArrayList<>();
        changerequestmessage = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        awaiteventadapter = new MyAwaitingEventsadapter(getActivity(), awaitingeventlist);
        recyclerView.setAdapter(awaiteventadapter);
        loading.setVisibility(View.GONE);
        scrollFunction();
        currentPageNumber = 1;
        MAX_SCROLLING_LIMIT=10;
        isAlreadyLoading = false;
        myawaitingevents(1);


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),
                recyclerView, new ClickListener() {

            @Override
            public void onClick(View view, final int position) {

                if (awaitingeventlist.get(position).getparticipantstype().equals("Open")) {

                } else if (awaitingeventlist.get(position).getChatlist().size() != 0 || awaitingeventlist.get(position).getChatlist() != null)

                {
                    participantname.clear();
                    participantid.clear();
                    participantemailid.clear();
                    acceptedstatus.clear();
                    changerequestmessage.clear();

                    for (int i = 0; i < awaitingeventlist.get(position).getChatlist().size(); i++) {
                        if (awaitingeventlist.get(position).getEventid().equals(awaitingeventlist.get(position).getChatlist().get(i).geteventid())) {
                            participantname.add(awaitingeventlist.get(position).getChatlist().get(i).getparticipantname());
                        }
                    }

                    for (int i = 0; i < awaitingeventlist.get(position).getChatlist().size(); i++) {
                        if (awaitingeventlist.get(position).getEventid().equals(awaitingeventlist.get(position).getChatlist().get(i).geteventid())) {
                            participantid.add(awaitingeventlist.get(position).getChatlist().get(i).getparticipantid());
                        }
                    }

                    for (int i = 0; i < awaitingeventlist.get(position).getChatlist().size(); i++) {
                        if (awaitingeventlist.get(position).getEventid().equals(awaitingeventlist.get(position).getChatlist().get(i).geteventid())) {
                            participantemailid.add(awaitingeventlist.get(position).getChatlist().get(i).getparticipantemailid());
                        }
                    }

                    for (int i = 0; i < awaitingeventlist.get(position).getChatlist().size(); i++) {
                        if (awaitingeventlist.get(position).getEventid().equals(awaitingeventlist.get(position).getChatlist().get(i).geteventid())) {
                            acceptedstatus.add(awaitingeventlist.get(position).getChatlist().get(i).getacceptedstatus());
                        }
                    }

                    for (int i = 0; i < awaitingeventlist.get(position).getChatlist().size(); i++) {
                        if (awaitingeventlist.get(position).getEventid().equals(awaitingeventlist.get(position).getChatlist().get(i).geteventid())) {
                            changerequestmessage.add(awaitingeventlist.get(position).getChatlist().get(i).getchangerequestmessage());
                        }
                    }
                    Intent intent = new Intent(getActivity(), EventParticipantActivity.class);
                    intent.putExtra("participantname", participantname);
                    intent.putExtra("participantid", participantid);
                    intent.putExtra("participantemailid", participantemailid);
                    intent.putExtra("acceptedstatus", acceptedstatus);
                    intent.putExtra("changerequestmessage", changerequestmessage);
                    startActivity(intent);

                }
            }

            @Override
            public void onLongClick(View view, int position) {


            }

        }));
        return view;
    }

    private void scrollFunction() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int last_seen_position = layoutManager.findLastVisibleItemPosition();
                Log.e("last_seen_position", last_seen_position + currentPageNumber + "");
                if (!isAlreadyLoading
                        && ((last_seen_position + 2) > MAX_SCROLLING_LIMIT)) {
                    Log.e("last_seen_position", last_seen_position + "" + currentPageNumber);
                    isAlreadyLoading = true;
                    MAX_SCROLLING_LIMIT += 10;
                    if (recyclerView != null) {
                        if (totalPages > currentPageNumber) {
                            currentPageNumber = currentPageNumber + 1;
                            loading.setVisibility(View.VISIBLE);
                            myawaitingevents(0);
                        }
                    }
                }
            }
        });
    }

    private void myawaitingevents(final int clear) {
        isAlreadyLoading = false;
        loading.setVisibility(View.GONE);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.myawaitingevents, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        totalPages = obj.getInt("totalpages");
                        currentPageNumber = obj.getInt("currentpageno");
                        if (clear == 1) {
                            awaitingeventlist.clear();
                        }
                        JSONArray userArray = obj.getJSONArray("eventslist");
                        for (int i = 0; i < userArray.length(); i++) {
                            AwaitingEventsResponse aer = new AwaitingEventsResponse();
                            JSONObject objusers = (JSONObject) userArray.get(i);
                            try {
                                aer.setEventdetailid(objusers.getString("eventdetailid"));
                                aer.setEventid(objusers.getString("eventid"));
                                aer.setEventtitle(objusers.getString("eventtitle"));
                                aer.setEventdate(objusers.getString("eventdate"));
                                aer.setEventduration(objusers.getString("eventduration"));
                                aer.setparticipantstype(objusers.getString("participantstype"));
                                aer.setnoofparticipants(objusers.getString("noofparticipants"));



                                JSONArray FoodNameArray = new JSONArray(userArray.getJSONObject(i).getString("participants"));
                                for (int j = 0; j < FoodNameArray.length(); j++) {
                                    String Foodname = FoodNameArray.getJSONObject(j).getString("participantname");
                                    String Foodname1 = FoodNameArray.getJSONObject(j).getString("participantid");
                                    String Foodname2 = FoodNameArray.getJSONObject(j).getString("participantemailid");
                                    String Foodname3 = FoodNameArray.getJSONObject(j).getString("acceptedstatus");
                                    String Foodname4 = FoodNameArray.getJSONObject(j).getString("changerequestmessage");
                                    AwaitingEventsResponse part = new AwaitingEventsResponse();
                                    AwaitingEventsResponse.participantss model = part.new participantss();
                                    model.setparticipantname(Foodname);
                                    model.setparticipantid(Foodname1);
                                    model.setparticipantemailid(Foodname2);
                                    model.setacceptedstatus(Foodname3);
                                    model.setchangerequestmessage(Foodname4);
                                    model.seteventid(objusers.getString("eventid"));
                                    l.add(model);
                                    aer.setChatlist(l);
                                }
                                awaitingeventlist.add(aer);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        awaiteventadapter.notifyDataSetChanged();
                    } else if (obj.getInt("status") == 0) {
                        tvErrorView.setVisibility(View.VISIBLE);
                    } else if (obj.getInt("status") == -1) {
                        MyApplication.getInstance().logout();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String name = MyApplication.getInstance().getPrefManager().getUser().getEmail();
                params.put("searchkey", "");
                params.put("token", name);
                params.put("pagenumber", currentPageNumber + "");
//                Log.e(TAG, "devicetype: " + name + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }


}
