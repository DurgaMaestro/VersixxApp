package org.appspot.apprtc.versixx.prim.Activities.UserVerification;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.MainActivity;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ForgotPassword extends Activity {
    EditText name;
    Button send;
    ImageView back;
    private String TAG = ForgotPassword.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot);
        name=(EditText)findViewById(R.id.name);
        back = (ImageView) findViewById(R.id.img);
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });
        send=(Button)findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(name.getText().toString().equals(""))
                {

                    Toast.makeText(getApplicationContext(), "Enter All Fields", Toast.LENGTH_LONG).show();
                    return;
                }

                else
                {
                    forgotPwd();
                }

            }
        });
    }
    private void forgotPwd() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.forgotpassword, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {

                        Toast.makeText(getApplicationContext(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();

                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                String namee = name.getText().toString();
                params.put("emailid", namee);



//                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}