package org.appspot.apprtc.versixx.prim.Activities;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import org.appspot.apprtc.ConnectActivity;
import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Adapter.navAdapter;
import org.appspot.apprtc.versixx.prim.Fragment.Events.AvailableopenEvents;
import org.appspot.apprtc.versixx.prim.Fragment.Events.CreateEvent;
import org.appspot.apprtc.versixx.prim.Fragment.Events.EventRequests;
import org.appspot.apprtc.versixx.prim.Fragment.Events.MyAwaitingEvents;
import org.appspot.apprtc.versixx.prim.Fragment.Friends.FriendClass;
import org.appspot.apprtc.versixx.prim.Fragment.Friends.FriendRequestClass;
import org.appspot.apprtc.versixx.prim.Fragment.Friends.SendFriendRequest;
import org.appspot.apprtc.versixx.prim.Fragment.HomePage.Home;
import org.appspot.apprtc.versixx.prim.Fragment.Messages;
import org.appspot.apprtc.versixx.prim.Fragment.Notifications;
import org.appspot.apprtc.versixx.prim.Fragment.Users.Profile;
import org.appspot.apprtc.versixx.prim.Fragment.Users.UpdatePassword;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.app.Myfunctions;
import org.appspot.apprtc.versixx.prim.modelclasses.navResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.bottomnavigation)
    AHBottomNavigation bottomNavigation;
    @Bind(R.id.drawer_layout)
    public
    DrawerLayout drawerLayout;
    @Bind(R.id.container)
    FrameLayout frameLayout;
    @Bind(R.id.drawer_list_left)
    RecyclerView recycler;
    @Bind(R.id.layoutLeftDrawer)
    public
    RelativeLayout layoutLeftDrawer;
    TextView apptitle;
    ImageView log;
    String token;
    @Bind(R.id.layoutDrawer)
    LinearLayout layoutdrawer;
    @Bind(R.id.navlout)
    Button navlout;
    Vector<navResponse> vector;
    String[] name;
    int check = 0;
    ActionBarDrawerToggle mDrawerToggle;
    LinearLayoutManager layoutmanager;
    ImageView navmenu;
    public ImageView search;
    ImageView back;
    View v;
    private String TAG = MainActivity.class.getSimpleName();
    public static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainactivity_3);
        ButterKnife.bind(this);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflator.inflate(R.layout.header1, null);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(v);
        vector = new Vector<>();
        layoutmanager = new LinearLayoutManager(this);
        layoutmanager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutmanager);
        drawerLayout.setDrawerListener(mDrawerToggle);
        context= getBaseContext().getApplicationContext();
        name = getResources().getStringArray(R.array.nav);
        loadRecyclerview();
        onClickfunctions();
        scrollFunction();
        navmenu = (ImageView) v.findViewById(R.id.navmenu);
        search = (ImageView) v.findViewById(R.id.lo);
        navmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (drawerLayout.isDrawerOpen(layoutLeftDrawer)) {
                    drawerLayout.closeDrawer(layoutLeftDrawer);
                } else {
                    drawerLayout.openDrawer(layoutLeftDrawer);
                    //ConnectActivity.RoomID="good";
                    //startActivity(new Intent(MainActivity.this, ConnectActivity.class));
                }

            }
        });


        addBottomtabs();
        Myfunctions.replaceFragmentwithoutBack(MainActivity.this, new Home());


        apptitle = (TextView) v.findViewById(R.id.apptitle);
        apptitle.setText(getResources().getString(R.string.app_name));
        log = (ImageView) v.findViewById(R.id.log);

        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(MainActivity.this, log);
                popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getTitle().equals("Logout")) {
                            logout();
                        }
                        if (item.getTitle().equals("Change Password")) {
                        }

                        return true;
                    }
                });
                popupMenu.show();
            }
        });

        navlout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Myfunctions.isNetworkAvailable(MainActivity.this)) ;
                {
                    logout();
                }

            }
        });

    }

    public void settitle(String ti) {
        getSupportActionBar().show();
        System.err.println("+++++++title++++++++" + ti + "+++++++++++++");
        TextView txttitle = (TextView) v.findViewById(R.id.apptitle);
        txttitle.setText(ti);
        ImageView txttitlee = (ImageView) v.findViewById(R.id.lo);
        txttitlee.setVisibility(View.GONE);
        System.err.println("+++++++title++++++++" + ti + "+++++++++++++");
    }
    public void setti() {

        ImageView txttitle = (ImageView) v.findViewById(R.id.lo);
        txttitle.setVisibility(View.VISIBLE);

    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }


    private void scrollFunction() {

    }

    public void Menus(View view) {
        PopupMenu popupMenu = new PopupMenu(MainActivity.this, view);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.Profile:
                        return true;
                    case R.id.update_pass:
                        Intent ii = new Intent(MainActivity.this, UpdatePassword.class);
                        startActivity(ii);
                        return true;
                    case R.id.logout:


                        return true;


                }
                return false;
            }

        });
        popupMenu.inflate(R.menu.popup_menu);
        popupMenu.show();

    }

    private void onClickfunctions() {


        recycler.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());

                if (child != null && gestureDetector2.onTouchEvent(e)) {
                    int position = rv.getChildLayoutPosition(child);
                    ListViewLeftItemOnClick(position);
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            }
        });
    }

    final GestureDetector gestureDetector2 = new GestureDetector(getBaseContext(), new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return true;
        }
    });

    private void ListViewLeftItemOnClick(int position) {
        if (vector.get(position).getName().equals("Send Friend Request")) {
            drawerLayout.closeDrawer(layoutLeftDrawer);
            MyApplication.repacefragment(MainActivity.this, new SendFriendRequest());

        }
        if (vector.get(position).getName().equals("Change Password")) {
            drawerLayout.closeDrawer(layoutLeftDrawer);
            MyApplication.repacefragment(MainActivity.this, new UpdatePassword());

        }
        if (vector.get(position).getName().equals("Create Events")) {
            drawerLayout.closeDrawer(layoutLeftDrawer);
            MyApplication.repacefragment(MainActivity.this, new CreateEvent());

        }
        if (vector.get(position).getName().equals("Event Requests")) {
            drawerLayout.closeDrawer(layoutLeftDrawer);
            MyApplication.repacefragment(MainActivity.this, new EventRequests());

        }
        if (vector.get(position).getName().equals("My Awaiting Events")) {
            drawerLayout.closeDrawer(layoutLeftDrawer);


            MyApplication.repacefragment(MainActivity.this, new MyAwaitingEvents());

        }
        if (vector.get(position).getName().equals("Available Open Events")) {
            drawerLayout.closeDrawer(layoutLeftDrawer);


            MyApplication.repacefragment(MainActivity.this, new AvailableopenEvents());

        }
        if (vector.get(position).getName().equals("Friend Requests")) {
            drawerLayout.closeDrawer(layoutLeftDrawer);
            MyApplication.repacefragment(MainActivity.this, new FriendRequestClass());

        }

    }

    private void logout() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.LOGOUT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getInt("status") == 1) {
                        MyApplication.getInstance().logout();
                        Toast.makeText(getApplicationContext(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getApplicationContext(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String name = MyApplication.getInstance().getPrefManager().getUser().getEmail();
                params.put("token", name);

                Log.e(TAG, "devicetype: " + name + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    public void addBottomtabs() {

        AHBottomNavigationItem item1 = new AHBottomNavigationItem("Messages", R.drawable.msg);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("My Events", R.drawable.myevents);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("My Friends", R.drawable.myfriends);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem("Notifications", R.drawable.notifications);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem("Profile", R.drawable.myprofile);
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#333333"));
        bottomNavigation.setAccentColor(Color.parseColor("#FF0000"));
        bottomNavigation.setInactiveColor(Color.parseColor("#ffffff"));
        bottomNavigation.setForceTint(true);
        bottomNavigation.setForceTitlesDisplay(true);
        bottomNavigation.setColored(false);
        bottomNavigation.setCurrentItem(1);
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position, boolean wasSelected) {

                if (position == 0) {
                    MyApplication.repacefragment(MainActivity.this, new Messages());
                } else if (position == 1) {
                    MyApplication.repacefragment(MainActivity.this, new Home());
                } else if (position == 2) {
                    MyApplication.repacefragment(MainActivity.this, new FriendClass());
                } else if (position == 3) {
                    MyApplication.repacefragment(MainActivity.this, new Notifications());
                } else if (position == 4) {
                    MyApplication.repacefragment(MainActivity.this, new Profile());
                }
            }
        });
    }

    private void loadRecyclerview() {
        TypedArray typedArray = getResources().obtainTypedArray(R.array.navimage);
        for (int i = 0; i < name.length; i++) {

            navResponse modelclass = new navResponse(name[i], typedArray.getResourceId(i, 0));
            vector.add(modelclass);
        }

        navAdapter adapter = new navAdapter(this, vector);
        recycler.setAdapter(adapter);


    }


}
