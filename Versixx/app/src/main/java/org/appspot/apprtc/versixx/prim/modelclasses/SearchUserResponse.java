package org.appspot.apprtc.versixx.prim.modelclasses;


public class SearchUserResponse
{
    public SearchUserResponse(String userid, String firstname, String lastname, String emailid, String contactnumber, String profileimagepath)
    {
        this.userid = userid;
        this.firstname = firstname;
        this.lastname = lastname;
        this.emailid = emailid;
        this.contactnumber = contactnumber;
        this.profileimagepath = profileimagepath;


    }
        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }

        public String getContactnumber() {
            return contactnumber;
        }

        public void setContactnumber(String contactnumber) {
            this.contactnumber = contactnumber;
        }

        public String getProfileimagepath() {
            return profileimagepath;
        }

        public void setProfileimagepath(String profileimagepath) {
            this.profileimagepath = profileimagepath;
        }


        private String userid;


        private String firstname;


        private String lastname;


        private String emailid;


        private String contactnumber;

        private String profileimagepath;




}
