package org.appspot.apprtc.versixx.prim.Activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.appspot.apprtc.BuildConfig;
import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.UserVerification.LoginActivity;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.app.Myfunctions;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Splashscreen extends Activity {
    String deviceId;
    private String TAG = Splashscreen.class.getSimpleName();
    Handler handler;
    ImageView imageView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        imageView=(ImageView)findViewById(R.id.imageView1);
        Glide.with(Splashscreen.this).load(R.drawable.flashscreen)

               // .centerCrop()
//                .crossFade()
                .into(imageView);
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                deviceId = Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                Log.e("DeviceId", "" + deviceId);
                String packagename = BuildConfig.APPLICATION_ID;
                String versionname = BuildConfig.VERSION_NAME;
                int versioncode = BuildConfig.VERSION_CODE;
                if (Myfunctions.isNetworkAvailable(Splashscreen.this)) {
                    check(deviceId, versioncode);
                }

            }
        }, 2000);
    }

    private void check(final String deviceId, final int versioncode) {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.checkdevice, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);


                    if (obj.getInt("status")== 0) {
                        String message = obj.getString("msg");
                        alert(message);
                    } else if (obj.getInt("status") == 1) {
                        Intent intent = new Intent(Splashscreen.this, LoginActivity.class);
                        intent.putExtra("usernamee", "" + obj.getString("username"));
                        startActivity(intent);
                        finish();
                    } else if (obj.getInt("status") == 2) {
                        Intent intent = new Intent(Splashscreen.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (obj.getInt("status") == 3) {

                        String message = obj.getString("msg");
                        alert(message);
                    } else {
                        Myfunctions.toastShort(Splashscreen.this, "Operation failed");
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("deviceid", deviceId);
                params.put("devicetype", String.valueOf("1"));
                params.put("versionnumber", String.valueOf(versioncode));
                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    public void alert(String message)
    {
        new AlertDialog.Builder(Splashscreen.this)
                .setTitle("Alert Message")
                .setMessage(message)
                .setPositiveButton(" Ok ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        Myfunctions.gotoGooglePlay(Splashscreen.this);
                        Splashscreen.this.finish();
                    }
                }).setNegativeButton(" Cancel ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                Splashscreen.this.finish();
            }
        }).setCancelable(false).show();
    }

}
