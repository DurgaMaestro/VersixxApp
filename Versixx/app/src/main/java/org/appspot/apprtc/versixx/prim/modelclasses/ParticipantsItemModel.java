package org.appspot.apprtc.versixx.prim.modelclasses;


public class ParticipantsItemModel {


    private String name;
    private String url;
    private String description;


    public ParticipantsItemModel() {
    }

    public ParticipantsItemModel(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public ParticipantsItemModel(String participantid, String participantname, String participantemailid, String participantprofileimage, String eventid)
    {
        this.participantid = participantid;
        this.participantname = participantname;
        this.participantemailid = participantemailid;
        this.participantprofileimage  =participantprofileimage ;
        this.eventid = eventid;
    }
    public String getUrl() {
        return url;
    }
    private String eventid;

    private String participantid;

    private String participantname;

    private String participantemailid;

    private String participantprofileimage ;

    public String geteventid() {
        return eventid;
    }

    public void seteventid(String eventid) {
        this.eventid = eventid;
    }

    public String getparticipantid() {
        return participantid;
    }

    public void setparticipantid(String participantid) {
        this.participantid = participantid;
    }

    public String getparticipantname() {
        return participantname;
    }

    public void setparticipantname(String participantname) {
        this.participantname = participantname;
    }

    public String getparticipantemailid() {
        return participantemailid;
    }

    public void setparticipantemailid(String participantemailid) {
        this.participantemailid = participantemailid;
    }

    public String getparticipantprofileimage() {
        return participantprofileimage;
    }

    public void setparticipantprofileimage(String participantprofileimage) {
        this.participantprofileimage = participantprofileimage;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
