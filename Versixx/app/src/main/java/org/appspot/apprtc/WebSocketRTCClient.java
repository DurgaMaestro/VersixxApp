/*
 *  Copyright 2014 The WebRTC Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

package org.appspot.apprtc;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.View;

import org.appspot.apprtc.RoomParametersFetcher.RoomParametersFetcherEvents;
import org.appspot.apprtc.WebSocketChannelClient.WebSocketChannelEvents;
import org.appspot.apprtc.WebSocketChannelClient.WebSocketConnectionState;
import org.appspot.apprtc.multipeer.BasePeerConnection;
import org.appspot.apprtc.multipeer.PeerConnectionClient;
import org.appspot.apprtc.multipeer.PeerConnectionListener;
import org.appspot.apprtc.util.AsyncHttpURLConnection;
import org.appspot.apprtc.util.AsyncHttpURLConnection.AsyncHttpEvents;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioTrack;
import org.webrtc.IceCandidate;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoRenderer;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;

/**
 * Negotiates signaling for chatting with https://appr.tc "rooms".
 * Uses the client<->server specifics of the apprtc AppEngine webapp.
 * <p>
 * <p>To use: create an instance of this object (registering a message handler) and
 * call connectToRoom().  Once room connection is established
 * onConnectedToRoom() callback with room parameters is invoked.
 * Messages to other party (with local Ice candidates and answer SDP) can
 * be sent after WebSocket connection is established.
 */
public class WebSocketRTCClient implements AppRTCClient, WebSocketChannelEvents {
    private static final String TAG = "WSRTCClient";
    private static final String ROOM_JOIN = "join";
    private static final String ROOM_MESSAGE = "message";
    private static final String ROOM_LEAVE = "leave";
    private String roomName;

    private enum ConnectionState {NEW, CONNECTED, CLOSED, ERROR}

    private enum MessageType {MESSAGE, LEAVE}

    private final Handler handler;
    private boolean initiator;
    private SignalingEvents events;
    private WebSocketChannelClient wsClient;
    private ConnectionState roomState;
    private RoomConnectionParameters connectionParameters;
    private String messageUrl;
    private String leaveUrl;

    private static String RoomID = ConnectActivity.RoomID;
//    private String remoteUserId = "";
//    private String userID = "";
    private String DESC = "";
    public static String clientID = MyApplication.getInstance().getPrefManager().getUser().getEmail();

    private HashMap<String, String> streamRemoteUserId = new HashMap<>();
    private HashMap<String, String> broadcastRemoteUserId = new HashMap<>();
    private Map<String, BasePeerConnection> broadcastPeerList = new HashMap<>();
    private Map<String, BasePeerConnection> streamPeerList = new HashMap<>();
    private Map<String, MediaStream> localStream = new HashMap<>();
    private Map<String, SurfaceViewRenderer> videoPlayer = new HashMap<>();


    public WebSocketRTCClient(SignalingEvents events) {
        this.events = events;
        roomState = ConnectionState.NEW;
        final HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    // --------------------------------------------------------------------
    // AppRTCClient interface implementation.
    // Asynchronously connect to an AppRTC room URL using supplied connection
    // parameters, retrieves room parameters and connect to WebSocket server.
    @Override
    public void connectToRoom(RoomConnectionParameters connectionParameters) {
        this.connectionParameters = connectionParameters;
        this.roomName = connectionParameters.roomId;
        handler.post(new Runnable() {
            @Override
            public void run() {
                connectToRoomInternal();
            }
        });
    }

    @Override
    public void disconnectFromRoom() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                disconnectFromRoomInternal();
                handler.getLooper().quit();
            }
        });
    }

    // Connects to room - function runs on a local looper thread.
    private void connectToRoomInternal() {
        String connectionUrl = getConnectionUrl(connectionParameters);
        Log.d(TAG, "Connect to room: " + connectionUrl);
        roomState = ConnectionState.NEW;
        wsClient = new WebSocketChannelClient(handler, this);

        RoomParametersFetcherEvents callbacks = new RoomParametersFetcherEvents() {
            @Override
            public void onSignalingParametersReady(final SignalingParameters params) {
                WebSocketRTCClient.this.handler.post(new Runnable() {
                    @Override
                    public void run() {
                        WebSocketRTCClient.this.signalingParametersReady(params);
                    }
                });
            }

            @Override
            public void onSignalingParametersError(String description) {
                WebSocketRTCClient.this.reportError(description);
            }
        };

        new RoomParametersFetcher(connectionUrl, null, callbacks).makeRequest();
    }

    // Disconnect from room and send bye messages - runs on a local looper thread.
    private void disconnectFromRoomInternal() {
        Log.d(TAG, "Disconnect. Room state: " + roomState);
        if (roomState == ConnectionState.CONNECTED) {
            Log.d(TAG, "Closing room.");
            sendPostMessage(MessageType.LEAVE, leaveUrl, null);
        }
        roomState = ConnectionState.CLOSED;
        if (wsClient != null) {
            wsClient.disconnect(true);
        }
    }

    // Helper functions to get connection, post message and leave message URLs
    private String getConnectionUrl(RoomConnectionParameters connectionParameters) {
        String url = connectionParameters.roomUrl + "/" + ROOM_JOIN + "/" + connectionParameters.roomId + "/" + clientID;

        Log.d(TAG, "getConnectionUrl:" + url);
        return url;
    }

    private String getMessageUrl(
            RoomConnectionParameters connectionParameters, SignalingParameters signalingParameters) {

        String URL = connectionParameters.roomUrl + "/" + ROOM_MESSAGE + "/" + connectionParameters.roomId
                + "/" + clientID;//signalingParameters.clientId;
        return URL;
    }

    private String getLeaveUrl(
            RoomConnectionParameters connectionParameters, SignalingParameters signalingParameters) {
        String URL = connectionParameters.roomUrl + "/" + ROOM_LEAVE + "/" + connectionParameters.roomId + "/"
                + clientID;//signalingParameters.clientId;
        return URL;
    }

    // Callback issued when room parameters are extracted. Runs on local
    // looper thread.
    private void signalingParametersReady(final SignalingParameters signalingParameters) {
        Log.d(TAG, "Room connection completed.");
        if (connectionParameters.loopback
                && (!signalingParameters.initiator || signalingParameters.offerSdp != null)) {
            reportError("Loopback room is busy.");
            return;
        }
        if (!connectionParameters.loopback && !signalingParameters.initiator
                && signalingParameters.offerSdp == null) {
            Log.w(TAG, "No offer SDP in room response.");
        }
        initiator = signalingParameters.initiator;
        messageUrl = getMessageUrl(connectionParameters, signalingParameters);
        leaveUrl = getLeaveUrl(connectionParameters, signalingParameters);
        Log.d(TAG, "Message URL: " + messageUrl);
        Log.d(TAG, "Leave URL: " + leaveUrl);
        roomState = ConnectionState.CONNECTED;

        // Fire connection and signaling parameters events.
        events.onConnectedToRoom(signalingParameters);

        // Connect and register WebSocket client.
        wsClient.connect(signalingParameters.wssUrl, signalingParameters.wssPostUrl);
        wsClient.register(connectionParameters.roomId, signalingParameters.clientId);
    }

    // Send local offer SDP to the other participant.
    @Override
    public void sendOfferSdp(final String broadcasterId, final SessionDescription sdp) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (roomState != ConnectionState.CONNECTED) {
                    reportError("Sending offer SDP in non connected state.");
                    return;
                }
                JSONObject json = new JSONObject();
                jsonPut(json, "type", "offer");
                jsonPut(json, "roomid", RoomID);
                jsonPut(json, "targetid", broadcastRemoteUserId.get(broadcasterId));
                jsonPut(json, "broadcasterid", broadcasterId);

                JSONObject offerJson = new JSONObject();
                jsonPut(offerJson, "type", "offer");
                jsonPut(offerJson, "sdp", sdp.description);
                jsonPut(json, "offer", offerJson);
//        sendPostMessage(MessageType.MESSAGE, messageUrl, json.toString());
                Log.e(TAG, "send offer......." + json.toString());
                wsClient.send(json.toString());
            }
        });
    }

    // Send local answer SDP to the other participant.
//    @Override
//    public void sendAnswer() {
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                JSONObject json = new JSONObject();
//                if (EndPoints.TYPE == EndPoints.CASTER) {
//
//                    jsonPut(json, "roomid", RoomID);
//                    jsonPut(json, "broadcasterid", userID);
//                    jsonPut(json, "answer", DESC);
//                    jsonPut(json, "targetid", remoteUserId);
//
//                } else if (EndPoints.TYPE == EndPoints.CASTRECEIVER) {
//                    jsonPut(json, "roomid", RoomID);
//                    jsonPut(json, "broadcasterid", userID);
//                    jsonPut(json, "answer", DESC);
//                    jsonPut(json, "targetid", remoteUserId);
//                }
//
//                jsonPut(json, "type", "answer");
//                Log.e(TAG, "Sending answer...." + json);
//                wsClient.send(json.toString());
//            }
//        });
//    }
//
//    @Override
//    public void broadCaster(final String broadcasterId) {//versixx
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//
//                JSONObject json = new JSONObject();
//                //jsonPut(json, "sdp", sdp.description);
//                jsonPut(json, "type", "offer");
//                jsonPut(json, "broadcasterid", broadcasterId);
//                jsonPut(json, "stargetid", remoteUserId);
//                jsonPut(json, "userid", clientID);
//                jsonPut(json, "roomid", RoomID);
//                Log.e(TAG, "Sending offer for type 2...." + json);
//                wsClient.send(json.toString());
//                //sendPostMessage(MessageType.MESSAGE, messageUrl, json.toString());
//            }
//        });
//    }


    private void startBroadcast(final String broadcasterId) {
//        if (userID != null)
        {
            Log.d(TAG, "Initiating broadcast..." + broadcasterId);
            Log.d(TAG, "Getting user media..");
            gotLocalStream(null, broadcasterId);
        }
    }

    public void gotLocalStream(final MediaStream stream, final String broadcasterId) {
        Log.d(TAG, "New broadcast peer: " + broadcasterId);
//        final String id = broadcasterId == null ? clientID : broadcasterId;
        final String id = broadcasterId;
        // Create broadcasting peer
        PeerConnectionClient.getInstance().createPeerConnection(id, new PeerConnectionListener() {
            @Override
            public void peerConnectionCreated(BasePeerConnection peerConnection) {
//                if(peerConnection == null) {
//                    return;
//                }
                broadcastPeerList.put(id, peerConnection);
                MediaStream stream1;
                if (stream == null) {
                    stream1 = PeerConnectionClient.getInstance().getLocalStream();
                } else {
                    stream1 = stream;
                }
                localStream.put(id, stream1);
                LinkedList<AudioTrack> audioTracks = localStream.get(id).audioTracks;
                if (audioTracks != null) {
                    for (AudioTrack audioTrack : audioTracks) {
                        localStream.get(id).removeTrack(audioTrack);
                    }
                }
                peerConnection.addStream(localStream.get(id));
                streamReady(id);
            }
        });
    }

    public void removeLocalStream(final MediaStream stream, final String broadcasterId) {
        Log.d(TAG, "New broadcast peer: " + broadcasterId);
//        final String id = broadcasterId == null ? clientID : broadcasterId;
        final String id = broadcasterId;
        // Create broadcasting peer
        PeerConnectionClient.getInstance().createPeerConnection(id, new PeerConnectionListener() {
            @Override
            public void peerConnectionCreated(BasePeerConnection peerConnection) {
//                if(peerConnection == null) {
//                    return;
//                }
                broadcastPeerList.remove(peerConnection);
                MediaStream stream1;
                if (stream == null) {
                    stream1 = PeerConnectionClient.getInstance().getLocalStream();
                } else {
                    stream1 = stream;
                }
                localStream.remove(stream1);
                LinkedList<AudioTrack> audioTracks = localStream.get(id).audioTracks;
                if (audioTracks != null) {
                    for (AudioTrack audioTrack : audioTracks) {
                        localStream.get(id).removeTrack(audioTrack);
                    }
                }
                peerConnection.removeStream();
                //streamReady(id);
            }
        });
    }

    @Override
    public void streamReady(final String userID) {//versixx
        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONObject json = new JSONObject();
                jsonPut(json, "type", "streamready");
                jsonPut(json, "broadcasterid", userID);
                jsonPut(json, "userid", clientID);
                jsonPut(json, "roomid", RoomID);
                Log.d(TAG, "Sending..." + json);
                wsClient.send(json.toString());
                //sendPostMessage(MessageType.MESSAGE, messageUrl, json.toString());

            }
        });
    }

    private void removeVideoPlayer(String broadcasterId) {
        SurfaceViewRenderer renderer = null;
        final CallActivity callActivity = (CallActivity) events;
        int i;
        for (i = 0; i < callActivity.remoteRenderers.size(); i++) {
            SurfaceViewRenderer remoteRenderScreen = (SurfaceViewRenderer) callActivity.remoteRenderers.get(i);
            try {
                Object getTag = remoteRenderScreen.getTag();
                Object id = (Object)broadcasterId;
                if (id.equals(getTag)) {
                    renderer = remoteRenderScreen;
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (renderer == null) {
            return;
        }
        final SurfaceViewRenderer selectedRenderer = renderer;
        final boolean multipleVideos = i > 0;
        ((CallActivity) events).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(multipleVideos) {
                    selectedRenderer.setVisibility(View.GONE);
                } else {
                    selectedRenderer.setVisibility(View.VISIBLE);
                }
            }
        });
        videoPlayer.remove(renderer);
    }

    private void addVideoPlayer(String broadcasterId) {
        SurfaceViewRenderer renderer = null;
        final CallActivity callActivity = (CallActivity) events;
        int i;
        for (i = 0; i < callActivity.remoteRenderers.size(); i++) {
            SurfaceViewRenderer remoteRenderScreen = (SurfaceViewRenderer) callActivity.remoteRenderers.get(i);
            if (remoteRenderScreen.getTag() == null && !broadcasterId.equals(remoteRenderScreen.getTag())) {
                renderer = remoteRenderScreen;
                remoteRenderScreen.setTag(broadcasterId);
                break;
            }
        }
        if (renderer == null) {
            return;
        }
        final SurfaceViewRenderer selectedRenderer = renderer;
        final boolean multipleVideos = i > 0;
        ((CallActivity) events).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(multipleVideos) {
                    for (int i = 0; i < callActivity.remoteRenderers.size(); i++) {
                        SurfaceViewRenderer remoteRenderScreen = (SurfaceViewRenderer) callActivity.remoteRenderers.get(i);
                        remoteRenderScreen.setVisibility(View.VISIBLE);
                    }
                } else {
                    selectedRenderer.setVisibility(View.VISIBLE);
                }
            }
        });
        videoPlayer.put(broadcasterId, renderer);
    }

    public void onAddStream(MediaStream stream, String broadcasterId, PeerConnection peerConnection) {
        Log.d(TAG, "Stream added");
        localStream.put(broadcasterId, stream);
        if (peerConnection == streamPeerList.get(broadcasterId).peerConnection) {
            gotLocalStream(stream, broadcasterId);
        }
    }

    private void startStream(JSONArray broadcasters) {
        if (broadcasters == null || broadcasters.length() == 0) {
            Log.d(TAG, "No Broadcasters found");
            return;
        }
        Log.d(TAG, "Waiting for broadcasters");
        for (int i = 0; i < broadcasters.length(); i++) {
            final String broadcasterId = broadcasters.optString(i);
            addVideoPlayer(broadcasterId);
            Log.d(TAG, "Initiating live stream for " + broadcasterId);

            PeerConnectionClient.getInstance().createPeerConnection(broadcasterId, new PeerConnectionListener() {
                @Override
                public void peerConnectionCreated(BasePeerConnection peerConnection) {
                    PeerConnectionClient.getInstance().startVideoSource();
                    SignalingParameters params = PeerConnectionClient.getInstance().getSignalingParameters();
                    if (params.iceCandidates != null) {
                        // Add remote ICE candidates from room.
                        for (IceCandidate iceCandidate : params.iceCandidates) {
                            PeerConnectionClient.getInstance().addRemoteIceCandidate(iceCandidate, peerConnection);
                        }
                    }
                    //TODO
//            peerConnection.setNegotiationListener(this);
                    streamPeerList.put(broadcasterId, peerConnection);
                    if (roomName != null && broadcasterId != null) {
                        getBroadcast(broadcasterId);
                    }
                }
            });
        }
    }


    @Override
    public void getBroadcast(final String broadcasterId) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONObject json = new JSONObject();
                jsonPut(json, "type", "getbroadcaster");
                jsonPut(json, "broadcasterid", broadcasterId);
                jsonPut(json, "userid", clientID);
                jsonPut(json, "roomid", RoomID);
                wsClient.send(json.toString());
                //sendPostMessage(MessageType.MESSAGE, messageUrl, json.toString());

            }
        });
    }

    @Override
    public void casterExit(final String broadcasterId) {
        handler.post(new Runnable() {
            @Override
            public void run() {

                JSONObject json = new JSONObject();

                Map.Entry<String, String> e = streamRemoteUserId.entrySet().iterator().next();
                String key = e.getKey().toString();

                jsonPut(json, "roomid", RoomID);
                jsonPut(json, "type", "exit");
                jsonPut(json, "broadcasterid", key);
                jsonPut(json, "userid", clientID);

                Log.e(TAG, "Sending casterExit...." + json);
                wsClient.send(json.toString());

                streamRemoteUserId.clear();

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        events.onChannelClose();
                    }
                });
            }
        });
    }

    @Override
    public void casterReceiverExit() {

        try {
            broadcastRemoteUserId.clear();
            streamRemoteUserId.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (Map.Entry<String, BasePeerConnection> e : broadcastPeerList.entrySet()) {
            Object key = e.getKey();

            try {
                broadcastPeerList.get(key).pcObserver=null;
                broadcastPeerList.get(key).removeStream();
                broadcastPeerList.get(key).peerConnection.close();
                broadcastPeerList.get(key).peerConnection.dispose();
                broadcastPeerList.remove(key);

            } catch (Exception e1) {
                e1.printStackTrace();
            }

        }

        if (broadcastPeerList.isEmpty()) {

            handler.post(new Runnable() {
                @Override
                public void run() {
                    events.onChannelClose();
                }
            });
        }

    }

    @Override
    public void sendAnswerSdp(final String broadcasterId, final SessionDescription sdp) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (connectionParameters.loopback) {
                    Log.e(TAG, "Sending answer in loopback mode.");
                    return;
                }
                JSONObject json = new JSONObject();
                jsonPut(json, "type", "answer");
                jsonPut(json, "roomid", RoomID);
                jsonPut(json, "targetid", streamRemoteUserId.get(broadcasterId));
                jsonPut(json, "broadcasterid", broadcasterId);
                JSONObject answerJson = new JSONObject();
                jsonPut(answerJson, "type", "answer");
                jsonPut(answerJson, "sdp", sdp.description);
                jsonPut(json, "answer", answerJson);

//                if (EndPoints.TYPE == EndPoints.CASTER) {
//                    jsonPut(json, "roomid", RoomID);
//                    jsonPut(json, "type", "answer");
//                    jsonPut(json, "broadcasterid", userID);
//                    jsonPut(json, "targetid", remoteUserId);
//
//                    JSONObject answerJson = new JSONObject();
//                    jsonPut(answerJson, "type", "answer");
//                    jsonPut(answerJson, "sdp", sdp.description);
//                    jsonPut(json, "answer", answerJson);
//
//                } else if (EndPoints.TYPE == EndPoints.CASTRECEIVER) {
//
//                    jsonPut(json, "roomid", RoomID);
//                    String type = "answer";
//                    jsonPut(json, "type", type);
//                    jsonPut(json, "broadcasterid", userID);
//                    jsonPut(json, "targetid", remoteUserId);
//
//                    JSONObject answerJson = new JSONObject();
//                    jsonPut(answerJson, "type", type);
//                    jsonPut(answerJson, "sdp", sdp.description);
//
//                    jsonPut(json, type, answerJson);
//                } else {
//                    jsonPut(json, "type", "answer");
//                }

                Log.e(TAG, "Sending answer...." + json);
                wsClient.send(json.toString());
            }
        });
    }

    // Send Ice candidate to the other participant.
    @Override
    public void sendLocalIceCandidate(final IceCandidate candidate, final BasePeerConnection basePeerConnection) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONObject json = new JSONObject();
                Log.e(TAG, "sendLocalIceCandidate.");
                if (!basePeerConnection.isInitiator) {
                    Log.e(TAG, "sendLocalIceCandidate..");
                    jsonPut(json, "type", "candidate");
                    jsonPut(json, "broadcasterid", broadcastRemoteUserId.get(basePeerConnection.broadcastId));
                    jsonPut(json, "roomid", RoomID);
                    jsonPut(json, "targetid", broadcastRemoteUserId.get(basePeerConnection.broadcastId));
                    jsonPut(json, "mode", "broadcast");

                    JSONObject candidateJson = new JSONObject();
                    jsonPut(candidateJson, "candidate", candidate.sdp);
                    jsonPut(candidateJson, "sdpMid", candidate.sdpMid);
                    jsonPut(candidateJson, "sdpMLineIndex", candidate.sdpMLineIndex);

                    jsonPut(json, "candidate", candidateJson);
                    Log.e(TAG, "sendLocalIceCandidate>");

                } else {

                    Log.e(TAG, "sendLocalIceCandidate...");
                    jsonPut(json, "type", "candidate");
                    jsonPut(json, "broadcasterid", basePeerConnection.broadcastId);
                    jsonPut(json, "roomid", RoomID);
                    jsonPut(json, "targetid", broadcastRemoteUserId.get(basePeerConnection.broadcastId));
                    jsonPut(json, "mode", "stream");
                    //jsonPut(json, "candidate", candidate.sdp);

                    JSONObject candidateJson = new JSONObject();

                    jsonPut(candidateJson, "candidate", candidate.sdp);
                    jsonPut(candidateJson, "sdpMid", candidate.sdpMid);
                    jsonPut(candidateJson, "sdpMLineIndex", candidate.sdpMLineIndex);

                    jsonPut(json, "candidate", candidateJson);
                    Log.e(TAG, "sendLocalIceCandidate>");
                }
                wsClient.send(json.toString());
            }
        });
    }

    // Send removed Ice candidates to the other participant.
    @Override
    public void sendLocalIceCandidateRemovals(final IceCandidate[] candidates, final BasePeerConnection peerConnection) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONObject json = new JSONObject();
                jsonPut(json, "type", "remove-candidates");
                JSONArray jsonArray = new JSONArray();
                for (final IceCandidate candidate : candidates) {
                    jsonArray.put(toJsonCandidate(candidate));
                }
                jsonPut(json, "candidates", jsonArray);
                if (initiator) {
                    // Call initiator sends ice candidates to GAE server.
                    if (roomState != ConnectionState.CONNECTED) {
                        reportError("Sending ICE candidate removals in non connected state.");
                        return;
                    }
                    sendPostMessage(MessageType.MESSAGE, messageUrl, json.toString());
                    if (connectionParameters.loopback) {
                        events.onRemoteIceCandidatesRemoved(candidates, peerConnection);
                    }
                } else {
                    // Call receiver sends ice candidates to websocket server.
                    wsClient.send(json.toString());
                }
            }
        });
    }

    // --------------------------------------------------------------------
    // WebSocketChannelEvents interface implementation.
    // All events are called by WebSocketChannelClient on a local looper thread
    // (passed to WebSocket client constructor).
    @Override
    public void onWebSocketMessage(final String msg) {
        if (wsClient.getState() != WebSocketConnectionState.REGISTERED) {
            Log.e(TAG, "Got WebSocket message in non registered state.");
            return;
        }
        //Log.d(TAG,"onWebSocketMessage :"+msg);
        Log.e(TAG, "onWebSocketMessage :" + msg);
        try {
            final JSONObject json = new JSONObject(msg);
            String msgText = json.getString("type");
            String errorText = json.optString("error");
            if (msgText.length() > 0) {
//                json = new JSONObject(msgText);
                String type = msgText;
                if (type.equals("candidate")) {
                    String mode = json.optString("mode");
                    String broadcasterid = json.optString("broadcasterid");
                    JSONObject candidate = json.optJSONObject("candidate");
                    BasePeerConnection peerConnection;
                    if (mode.equals("stream")) {
                        peerConnection = broadcastPeerList.get(broadcasterid);
                    } else {
                        peerConnection = streamPeerList.get(broadcasterid);
                    }
                    PeerConnectionClient.getInstance().addRemoteIceCandidate(toJavaCandidate(candidate), peerConnection);
//                    events.onRemoteIceCandidate(toJavaCandidate(json));
                } else if (type.equals("remove-candidates")) {
                    removeICECandidate(json);
                } else if (type.equals("answer")) {
                    setRemoteAnswerDesc(json, type);
                } else if (type.equals("offer")) {
                    setRemoteOfferDesc(json, type);
                } else if (type.equals("bye")) {
                    events.onChannelClose();
                } else if (type.equals("broadcastready")) {
                    if (EndPoints.TYPE == EndPoints.CASTER) {
                        String userID = json.getString("userid");
                        addVideoPlayer(userID);
                        startBroadcast(userID);
                    }
                } else if (type.equals("broadcasterjoined")) {
                    Log.d(TAG, "New Broadcaster Joined");
                    String broadcasterId = json.optString("broadcasterid");
                    if(streamPeerList.get(broadcasterId) == null) {
                        addBroadcast(broadcasterId);
//                        getBroadcast(broadcasterId);
                    }
                    //}
                } else if (type.equals("roomjoined")) {
//                    if (EndPoints.TYPE == EndPoints.CASTRECEIVER)
                    {
//                        String[] IDS = json.getString("broadcasters").replace("[", "").replace("]", "").replace("\"", "").split(",");
//                        if (IDS.length == 1) {
//                            userID = IDS[0];
//                        }
                        startStream(json.optJSONArray("broadcasters"));
//                        PeerConnectionClient.getInstance().createPeerConnection(new PeerConnectionListener() {
//                            @Override
//                            public void peerConnectionCreated(PeerConnection peerConnection) {
//                                json.optString("")
//                                getBroadcast();
//                            }
//                        });
                    }
                } else if (type.equals("userjoined")) {

                } else if (type.equals("waitforbroadcaster")) {
                    //events.onChannelClose();
                } else if (type.equals("userleft")) {//
//                    events.onChannelClose();
                } else if (type.equals("exit")) {//
                    final String broadcasterId = json.optString("broadcasterid");
                    if(broadcasterId!=null){
                        if(broadcastRemoteUserId.containsKey(broadcasterId)) {
                            broadcastRemoteUserId.remove(broadcasterId);
                        }
                        if(streamRemoteUserId.containsKey(broadcasterId)) {
                            streamRemoteUserId.remove(broadcasterId);
                        }

                        if(broadcastPeerList.containsKey(broadcasterId)) {
                            broadcastPeerList.get(broadcasterId).pcObserver = null;
                            broadcastPeerList.get(broadcasterId).removeStream();
                            broadcastPeerList.get(broadcasterId).peerConnection.close();
                            broadcastPeerList.get(broadcasterId).peerConnection.dispose();
                            broadcastPeerList.remove(broadcasterId);
                        }

                        if(broadcastPeerList.isEmpty()){

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    events.onChannelClose();
                                }
                            });
                        }else{
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                            removeVideoPlayer(broadcasterId);
                            //removeLocalStream(null,broadcasterId);
                                }
                            });
                        }

                    }

                } else if (type.equals("broadcasterexit")) {//
                    // events.onChannelClose();
                } else if (type.equals("streamerexit")) {
                    String broadcasterId = json.optString("broadcasterid");
                    if(broadcastRemoteUserId.containsKey(broadcasterId)) {
                        broadcastRemoteUserId.remove(broadcasterId);
                    }
                    if(streamRemoteUserId.containsKey(broadcasterId)) {
                        streamRemoteUserId.remove(broadcasterId);
                    }
                    updateBroadcastPeer(broadcasterId);

//                    events.onChannelClose();
                } else if (type.equals("broadcaster")) {
//                    remoteUserId = json.getString("remoteuserid");
                    Log.d(TAG, "remoteUserId::::::" + json.optString("remoteuserid"));
                    broadcastRemoteUserId.put(json.optString("broadcasterid"), json.optString("remoteuserid"));
                    BasePeerConnection connection = streamPeerList.get(json.optString("broadcasterid"));
                    PeerConnectionClient.getInstance().createOffer(connection);
                } else if (type.equals("receiver")) {
//                    remoteUserId = json.getString("remoteuserid");
                    streamRemoteUserId.put(json.optString("broadcasterid"), json.optString("remoteuserid"));
                } else {

                }
            }
        } catch (JSONException e) {
            reportError("WebSocket message JSON parsing error: " + e.toString());
        }
    }

    private void updateBroadcastPeer(final String broadcasterId) {
        //TODO:
        if(broadcastPeerList.containsKey(broadcasterId)) {
            broadcastPeerList.get(broadcasterId).pcObserver = null;
            broadcastPeerList.get(broadcasterId).removeStream();
            broadcastPeerList.get(broadcasterId).peerConnection.close();
            broadcastPeerList.get(broadcasterId).peerConnection.dispose();
            broadcastPeerList.remove(broadcasterId);
        }
        PeerConnectionClient.getInstance().createPeerConnection(broadcasterId, new PeerConnectionListener() {
            @Override
            public void peerConnectionCreated(BasePeerConnection peerConnection) {
                broadcastPeerList.put(broadcasterId, peerConnection);
                peerConnection.addStream(localStream.get(broadcasterId));
                streamReady(broadcasterId);
            }
        });
    }

    public void hangup(final String broadcasterid) {
        Log.d(TAG, "HANGUP " + broadcasterid);
        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONObject json = new JSONObject();
                jsonPut(json, "type", "exit");
                jsonPut(json, "broadcasterid", broadcasterid);
                jsonPut(json, "userid", clientID);
                jsonPut(json, "roomid", RoomID);
                Log.d(TAG, "Sending..." + json);
                wsClient.send(json.toString());
            }
        });
    }

    private void addBroadcast(String broadcasterId) {
        JSONObject json = new JSONObject();
        jsonPut(json, "type", "joinbroadcast");
        jsonPut(json, "roomid", RoomID);
        jsonPut(json, "userid", clientID);
        jsonPut(json, "broadcasterid", broadcasterId);
        wsClient.send(json.toString());
    }


    private void setRemoteAnswerDesc(JSONObject json, String type) {
        SessionDescription sdp = new SessionDescription(
                SessionDescription.Type.fromCanonicalForm(type),
                json.optJSONObject("answer").optString("sdp"));
        final BasePeerConnection peerConnection = streamPeerList.get(json.optString("broadcasterid"));
        PeerConnectionClient.getInstance().setRemoteDescription(sdp, peerConnection, new SdpObserver() {
            @Override
            public void onCreateSuccess(SessionDescription sessionDescription) {

            }

            @Override
            public void onSetSuccess() {
                PeerConnectionClient.getInstance().drainCandidates(peerConnection);
            }

            @Override
            public void onCreateFailure(String s) {

            }

            @Override
            public void onSetFailure(String s) {

            }
        });
    }

    private void setRemoteOfferDesc(JSONObject json, String type) {
        JSONObject json1 = json.optJSONObject("offer");
        SessionDescription sdp = new SessionDescription(
                SessionDescription.Type.fromCanonicalForm(type), json1.optString("sdp"));
        final BasePeerConnection peerConnection = broadcastPeerList.get(json.optString("broadcasterid"));
        peerConnection.isInitiator = false;
        peerConnection.peerConnection.setRemoteDescription(new SdpObserver() {
            @Override
            public void onCreateSuccess(SessionDescription sessionDescription) {
                Log.d(TAG, "onCreateSuccess");
            }

            @Override
            public void onSetSuccess() {
                PeerConnectionClient.getInstance().createAnswer(peerConnection);
            }

            @Override
            public void onCreateFailure(String s) {

            }

            @Override
            public void onSetFailure(String s) {

            }
        }, sdp);
//        PeerConnectionClient.getInstance().createAnswer(peerConnection);
    }

    private void removeICECandidate(JSONObject json) {
        try {
            String mode = json.optString("mode");
            String broadcasterid = json.optString("broadcasterid");
            JSONArray candidateArray = json.optJSONArray("candidates");
            IceCandidate[] candidates = new IceCandidate[candidateArray.length()];

            for (int i = 0; i < candidateArray.length(); ++i) {
                candidates[i] = toJavaCandidate(candidateArray.optJSONObject(i));
            }

            BasePeerConnection peerConnection;
            if (mode.equals("stream")) {
                peerConnection = broadcastPeerList.get(broadcasterid);
            } else {
                peerConnection = streamPeerList.get(broadcasterid);
            }
            PeerConnectionClient.getInstance().removeRemoteIceCandidates(candidates, peerConnection);
        } catch (Exception e) {

        }
    }


    @Override
    public void onWebSocketClose() {
        events.onChannelClose();
    }

    @Override
    public void onWebSocketError(String description) {
        reportError("WebSocket error: " + description);
    }

    // --------------------------------------------------------------------
    // Helper functions.

    private void reportError(final String errorMessage) {
        Log.e(TAG, errorMessage);
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (roomState != ConnectionState.ERROR) {
                    roomState = ConnectionState.ERROR;
                    events.onChannelError(errorMessage);
                }
            }
        });
    }

    // Put a |key|->|value| mapping in |json|.
    private static void jsonPut(JSONObject json, String key, Object value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    // Send SDP or ICE candidate to a room server.
    private void sendPostMessage(
            final MessageType messageType, final String url, final String message) {
        String logInfo = url;
        if (message != null) {
            logInfo += ". Message: " + message;
        }
        Log.d(TAG, "C->GAE: " + logInfo);
        AsyncHttpURLConnection httpConnection =
                new AsyncHttpURLConnection("POST", url, message, new AsyncHttpEvents() {
                    @Override
                    public void onHttpError(String errorMessage) {
                        reportError("GAE POST error: " + errorMessage);
                    }

                    @Override
                    public void onHttpComplete(String response) {
                        if (messageType == MessageType.MESSAGE) {
                            try {
                                JSONObject roomJson = new JSONObject(response);
                                String result = roomJson.getString("result");
                                if (!result.equals("SUCCESS")) {
                                    reportError("GAE POST error: " + result);
                                }
                            } catch (JSONException e) {
                                reportError("GAE POST JSON error: " + e.toString());
                            }
                        }
                    }
                });
        httpConnection.send();
    }

    // Converts a Java candidate to a JSONObject.
    private JSONObject toJsonCandidate(final IceCandidate candidate) {
        JSONObject json = new JSONObject();
        jsonPut(json, "label", candidate.sdpMLineIndex);
        jsonPut(json, "id", candidate.sdpMid);
        jsonPut(json, "candidate", candidate.sdp);
        return json;
    }

    // Converts a JSON candidate to a Java object.
    IceCandidate toJavaCandidate(JSONObject candidate) throws JSONException {

//        JSONObject candidate = json.optJSONObject("candidate");
        return new IceCandidate(candidate.optString("sdpMid"), candidate.optInt("sdpMLineIndex"), candidate.getString("candidate"));
//        return new IceCandidate(json.getString("broadcasterid"), json.optInt("label"), json.getString("candidate"));


//        return new IceCandidate(
//                json.getString("id"), json.getInt("label"), json.getString("candidate"));
    }

    public Map<String, BasePeerConnection> getStreamPeerList() {
        return streamPeerList;
    }

    public Map<String, BasePeerConnection> getBroadcastPeerList() {
        return broadcastPeerList;
    }

}
