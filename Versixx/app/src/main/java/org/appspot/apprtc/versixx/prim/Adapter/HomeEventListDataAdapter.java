package org.appspot.apprtc.versixx.prim.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.modelclasses.ParticipantsItemModel;

import java.util.ArrayList;

public class HomeEventListDataAdapter extends RecyclerView.Adapter<HomeEventListDataAdapter.SingleItemRowHolder> {

    private ArrayList<ParticipantsItemModel> itemsList;
    private Context mContext;

    public HomeEventListDataAdapter(Context context, ArrayList<ParticipantsItemModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_single_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        ParticipantsItemModel singleItem = itemsList.get(i);

        holder.tvTitle.setText(singleItem.getparticipantname());
//        Picasso.with(mContext)
//                .load(EndPoints.IMAGE_BASE_URL +  singleItem.getparticipantprofileimage())
//                .resize(125,125)
//                .into(holder.itemImage);
        Glide.with(mContext).load(EndPoints.IMAGE_BASE_URL +  singleItem.getparticipantprofileimage())

                .centerCrop()
//                .crossFade()
                .into(holder.itemImage);

//       Glide.with(mContext)
//                .load(EndPoints.IMAGE_BASE_URL + singleItem.getparticipantprofileimage())
//
//                .centerCrop()
//        .into(holder.itemImage);
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;

        protected ImageView itemImage;


        public SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);


//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
//                    Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();
//
//                }
//            });


        }

    }

}