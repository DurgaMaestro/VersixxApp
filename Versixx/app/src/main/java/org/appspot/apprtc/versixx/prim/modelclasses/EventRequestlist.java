package org.appspot.apprtc.versixx.prim.modelclasses;

public class EventRequestlist {

    public EventRequestlist(String eventrequestid, String eventid, String senderprofileimg, String eventtitle, String eventdate, String eventduration)
    {
        this.eventrequestid = eventrequestid;
        this.eventid = eventid;
        this.senderprofileimg = senderprofileimg;
        this.eventtitle = eventtitle;
        this.eventdate = eventdate;
        this.eventduration = eventduration;


    }

    private String eventrequestid;

    private String eventid;


    private String senderprofileimg;


    private String eventtitle;

    private String eventdate;

    private String eventduration;

    public String getEventrequestid() {
        return eventrequestid;
    }

    public void setEventrequestid(String eventrequestid) {
        this.eventrequestid = eventrequestid;
    }

    public String getEventid() {
        return eventid;
    }

    public void setEventid(String eventid) {
        this.eventid = eventid;
    }

    public String getSenderprofileimg() {
        return senderprofileimg;
    }

    public void setSenderprofileimg(String senderprofileimg) {
        this.senderprofileimg = senderprofileimg;
    }

    public String getEventtitle() {
        return eventtitle;
    }

    public void setEventtitle(String eventtitle) {
        this.eventtitle = eventtitle;
    }

    public String getEventdate() {
        return eventdate;
    }

    public void setEventdate(String eventdate) {
        this.eventdate = eventdate;
    }

    public String getEventduration() {
        return eventduration;
    }

    public void setEventduration(String eventduration) {
        this.eventduration = eventduration;
    }
}
