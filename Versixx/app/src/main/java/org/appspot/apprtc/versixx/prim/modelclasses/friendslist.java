package org.appspot.apprtc.versixx.prim.modelclasses;


public class friendslist {
    public friendslist(String friendid, String friendprofileimage, String friendname, String friendemailid, String friendlivesin) {
        this.friendid = friendid;
        this.friendprofileimage = friendprofileimage;
        this.friendname = friendname;
        this.friendemailid = friendemailid;
        this.friendlivesin = friendlivesin;
    }
        private String friendid	,friendprofileimage,friendname,friendemailid,friendlivesin;

        public String getfriendid() {
            return friendid;
        }
    public void setfriendlivesin(String friendlivesin) {
        this.friendlivesin = friendlivesin;
    }
    public String getfriendlivesin() {
        return friendlivesin;
    }
        public void setfriendid(String friendid) {
            this.friendid = friendid;
        }
    public String getfriendprofileimage() {
        return friendprofileimage;
    }

    public void setfriendprofileimage(String friendprofileimage) {
        this.friendprofileimage = friendprofileimage;
    }
    public String getfriendname() {
        return friendname;
    }

    public void setfriendname(String friendname) {
        this.friendname = friendname;
    }
    public String getfriendemailid() {
        return friendemailid;
    }

    public void setfriendemailid(String friendemailid) {
        this.friendemailid = friendemailid;
    }


}
