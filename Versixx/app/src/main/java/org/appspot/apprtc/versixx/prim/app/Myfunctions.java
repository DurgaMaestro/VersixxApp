package org.appspot.apprtc.versixx.prim.app;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;


import org.appspot.apprtc.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Myfunctions
{


    static String app_name = "com.app.versixx.versixx";

    public static void repacefragment(AppCompatActivity context, Fragment fragment)
    {
        FragmentManager fragmentManager = context.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container,fragment).commit();
    }

    public static void replaceacivity(AppCompatActivity context, Class aClass)
    {
        Intent intent = new Intent(context,aClass);
        context.startActivity(intent);
    }
    public static boolean isEmptytext(EditText editText, String text)
    {

        boolean status = editText.getText().toString().equals("");
        if(status)
        {
            editText.setError(text+" can not be Empty ");
        }
        else {
            editText.setError(null);
        }
        return status;
    }

    public  static  boolean emailValidator(Context context, String email, EditText et)
    {

        Pattern pattern;
        Matcher matcher;
        final String emailpattern="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(emailpattern);
        matcher = pattern.matcher(email);
        return matcher.matches();


    }
    public static void setSharedPrefs(Context c, String key, String value) {

        SharedPreferences.Editor editor = c.getSharedPreferences(app_name,
                Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();

    }

    public static void setSharedPrefs(Context c, String key, int value) {
        if (c != null) {
            SharedPreferences.Editor editor = c.getSharedPreferences(app_name,
                    Context.MODE_PRIVATE).edit();
            editor.putInt(key, value);
            editor.commit();
        }
    }

    public static String getSharedPrefs(Context c, String key,
                                        String default_value) {
        if (c == null)
        {
            return default_value;
        } else {
            SharedPreferences prefs = c.getSharedPreferences(app_name,
                    Context.MODE_PRIVATE);

            return prefs.getString(key, default_value);
        }
    }

    public static int getSharedPrefs(Context c, String key, int default_value) {
        if (c == null) {
            return default_value;
        } else {
            SharedPreferences prefs = c.getSharedPreferences(app_name,
                    Context.MODE_PRIVATE);
            return prefs.getInt(key, default_value);
        }
    }

    public static boolean isNetworkAvailable(Activity c) {
        boolean state;
        ConnectivityManager cmg = (ConnectivityManager) c
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cmg.getActiveNetworkInfo();
        state = activeNetworkInfo != null && activeNetworkInfo.isConnected();

        if (state) {
            return true;
        } else {
            toastShort(c,
                    "No Internet Connection Available! Please Check your Connection");
            return false;
        }
    }
    public static void replaceFragment(AppCompatActivity c, Fragment fragment) {
        FragmentManager fragmentManager = c.getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public static void addFragment(AppCompatActivity c, Fragment fragment) {
        FragmentManager fragmentManager = c.getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    public static void replaceFragmentwithoutBack(FragmentActivity c, Fragment fragment)
    {
//        c.recreate();
        FragmentManager fragmentManager = c.getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }
    public static void toastShort(Context c, String msg) {
        if (c != null && msg != null)
            Toast.makeText(c, msg, Toast.LENGTH_SHORT).show();
    }

    public static void gotoGooglePlay(Context c) {
        final String appPackageName = c.getPackageName(); // getPackageName()
        // from Context or
        // Activity object

        Log.e("Pack", "" + appPackageName);
        try {
            c.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("market://details?id=" + appPackageName)));
        } catch (ActivityNotFoundException anfe) {
            c.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("http://play.google.com/store/apps/details?id="
                            + appPackageName)));
        }

    }
}
