package org.appspot.apprtc.versixx.prim.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


import com.wang.avi.AVLoadingIndicatorView;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Adapter.EventParticipantsdapter;
import org.appspot.apprtc.versixx.prim.modelclasses.ParticipantssResponse;

import java.util.ArrayList;
import java.util.Vector;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EventParticipantActivity extends AppCompatActivity {


    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    ArrayList<String> arrayList = new ArrayList<>();
    Vector<ParticipantssResponse> vector ;
    @Bind(R.id.loading_bar)
    AVLoadingIndicatorView loading;
    @Bind(R.id.tvErrorView)
    TextView errorView;
    int currentPageNumber = 1, totalPages;
    private int MAX_SCROLLING_LIMIT = 10;
    private boolean isAlreadyLoading;
    EventParticipantsdapter adapter;
    ArrayList<String> participantnamelist;
    ArrayList<String> participantemailidlist;
    ArrayList<String> acceptedstatuslist;
    ArrayList<String> changerequestmessagelist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.genericrecyclerview1);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("Participants");
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        loading.setVisibility(View.GONE);
        arrayList = intent.getStringArrayListExtra("participantname");
        arrayList = intent.getStringArrayListExtra("participantid");
        arrayList = intent.getStringArrayListExtra("participantemailid");
        arrayList = intent.getStringArrayListExtra("acceptedstatus");
        arrayList = intent.getStringArrayListExtra("changerequestmessage");
        vector = new Vector<>();
        participantnamelist = (ArrayList<String>) getIntent().getSerializableExtra("participantname");
        ArrayList<String> participantidlist = (ArrayList<String>) getIntent().getSerializableExtra("participantid");
        participantemailidlist = (ArrayList<String>) getIntent().getSerializableExtra("participantemailid");
          acceptedstatuslist = (ArrayList<String>) getIntent().getSerializableExtra("acceptedstatus");
        changerequestmessagelist = (ArrayList<String>) getIntent().getSerializableExtra("changerequestmessage");
        for(int i=0;i<participantnamelist.size();i++)
        {
            ParticipantssResponse modelclass = new ParticipantssResponse();
            modelclass.setParticipantname(participantnamelist.get(i));
            modelclass.setAcceptedstatus(acceptedstatuslist.get(i));
            modelclass.setParticipantemail(participantemailidlist.get(i));
            modelclass.setChangerequestmessage(changerequestmessagelist.get(i));
            modelclass.setTitle(" Participant "+(i+1));
            modelclass.setParticipantid(participantidlist.get(i));
            vector.add(modelclass);
        }
        adapter = new EventParticipantsdapter(this,vector);
        recyclerView.setAdapter(adapter);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id =item.getItemId();
        switch (id)
        {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

}
