package org.appspot.apprtc.versixx.prim.Activities.UserVerification;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.MainActivity;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.modelclasses.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements
        View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener {

    LoginButton login;
    ImageView back;
    ImageView signin;
    ImageView btnLogin;
    RelativeLayout activitymain;
    private SignInButton btnSignIn;
    TextView changepwd;
    private EditText inputName, inputEmail;
    ImageView background;
    private Button btnEnter;
    private String TAG = LoginActivity.class.getSimpleName();
    Context context = this;
    String username, prefname, dname, logoutname;
    List<String> langlist = new ArrayList<String>();
    List<String> langid = new ArrayList<String>();
    Spinner languagespinner;
    private static final int RC_SIGN_IN = 007;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;
    CallbackManager callbackManager;
    private String facebook_id, f_name, m_name, l_name, gender, profile_image, full_name, email_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.login);
        ButterKnife.bind(this);
        if (MyApplication.getInstance().getPrefManager().getUser() != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        Intent intent = getIntent();
        username = intent.getStringExtra("usernamee");
        inputName = (EditText) findViewById(R.id.eName);
        changepwd = (TextView) findViewById(R.id.clicdkhere);
        inputEmail = (EditText) findViewById(R.id.ePassword);
        languagespinner = (Spinner) findViewById(R.id.langspinner);
        btnEnter = (Button) findViewById(R.id.login);
        signin = (ImageView) findViewById(R.id.google);
        btnLogin = (ImageView) findViewById(R.id.fb);
        btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);
        background=(ImageView)findViewById(R.id.background);
        btnSignIn.setOnClickListener(this);
        inputName.addTextChangedListener(new MyTextWatcher(inputName));
        inputEmail.addTextChangedListener(new MyTextWatcher(inputEmail));


        Glide.with(LoginActivity.this).load(R.drawable.background)

                //.centerCrop()
//                .crossFade()
                .into(background);


        fetchlang();
        if (username != null && !username.equals("")) {
            inputName.setText(username);
        }
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        btnSignIn.setSize(SignInButton.SIZE_STANDARD);
        btnSignIn.setScopes(gso.getScopeArray());
        callbackManager = CallbackManager.Factory.create();
        login = (LoginButton) findViewById(R.id.login_button);
        login.setReadPermissions("public_profile email");
        getKeyHash();
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
                if (opr.isDone()) {
                    signOut();
                }
                signIn();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();

                }

                login.performClick();

                login.setPressed(true);

                login.invalidate();

                login.registerCallback(callbackManager, mCallBack);


            }
        });
        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
        changepwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, ForgotPassword.class);
                startActivity(i);
            }
        });
    }

    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {


            facebook_id = f_name = m_name = l_name = gender = profile_image = full_name = email_id = "";

            if (AccessToken.getCurrentAccessToken() != null) {
                RequestData();
                Profile profile = Profile.getCurrentProfile();
                if (profile != null) {
                    facebook_id = profile.getId();
//                    Log.e("facebook_id", facebook_id);
                    f_name = profile.getFirstName();
//                    Log.e("f_name", f_name);
                    m_name = profile.getMiddleName();
//                    Log.e("m_name", m_name);
                    l_name = profile.getLastName();
//                    Log.e("l_name", l_name);
                    full_name = profile.getName();
//                    Log.e("full_name", full_name);
//                    Log.e("profile_image", profile_image);
                }
            }

        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    private void getKeyHash() {

        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.androidlift.facebookdemo", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
//                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
//            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
//            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
//            Log.e("exception", e.toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();

        }
    }

    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                JSONObject json = response.getJSONObject();
                System.out.println("Json data :" + json);
                try {
                    if (json != null) {
                        String text = "<b>Name :</b> " + json.getString("name") + "<br><br><b>Email :</b> " + json.getString("email") + "<br><br><b>Profile link :</b> " + json.getString("link");
                        finish();
                        Intent i = new Intent(LoginActivity.this, SignupActivity.class);
                        i.putExtra("name", json.optString("name"));
                        i.putExtra("email", json.optString("email"));
                        startActivity(i);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        updateUI(false);
                    }
                });
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        updateUI(false);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
//        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.e(TAG, "display name: " + acct.getDisplayName());
            Log.e(TAG, "display name: " + acct.getPhotoUrl());
            String personName = acct.getDisplayName();
            String email = acct.getEmail();
            Intent i = new Intent(LoginActivity.this, SignupActivity.class);
            i.putExtra("name", personName);
            i.putExtra("email", email);
            startActivity(i);
            if (acct.getPhotoUrl() != null && !acct.getPhotoUrl().toString().equals("")) {
                String personPhotoUrl = acct.getPhotoUrl().toString();
                Glide.with(getApplicationContext()).load(personPhotoUrl)
                        .thumbnail(0.5f)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL);

            }
            updateUI(true);
        } else {
            updateUI(false);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_sign_in:
                OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
                if (opr.isDone()) {
                    signOut();
                }
                signIn();
                break;
        }
    }

    public void onDestroy() {

        super.onDestroy();

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

//        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }


    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            btnSignIn.setVisibility(View.GONE);
        } else {
            btnSignIn.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;

    }

    public void signup(View view) {

        Intent i = new Intent(LoginActivity.this, SignupActivity.class);
        i.putExtra("name", "");
        i.putExtra("email", "");

        startActivity(i);
    }

    private void login() {
        if (!validateName()) {
            return;
        }

        if (!validateEmail()) {
            return;
        }

        final String name = inputName.getText().toString();
        final String email = inputEmail.getText().toString();

        //Receiver
//        final String name = "ram@technotackle.com";
//        final String email = "123456";

        //Broadcaster
//        final String name = "om@themaestro.in ";
//        final String email = "12345";


        final String m_androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {

                        User user = new User(inputName.getText().toString(),
                                obj.getString("name"),
                                obj.getString("token"));

                        MyApplication.getInstance().getPrefManager().storeUser(user);
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", name);
                params.put("password", email);
                params.put("deviceip", MyApplication.get_ip(context));
                params.put("devicetype", String.valueOf("1"));
                params.put("deviceid", m_androidId);

                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }


    // Validating Name
    private boolean validateName() {
        String pwd = inputEmail.getText().toString().trim();
        if (pwd.isEmpty()) {
            inputEmail.setError("Enter the password");
            return false;
        } else {
            inputEmail.setError(null);
        }

        return true;
    }

    private void fetchlang() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.lang, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        JSONArray langArray = obj.getJSONArray("languagelist");
                        for (int i = 0; i < langArray.length(); i++) {
                            JSONObject langObj = (JSONObject) langArray.get(i);
                            langlist.add(langObj.getString("name"));
                            langid.add(langObj.getString("id"));
//                            Log.e(TAG, "json parsing error: " + langObj.getString("id"));

                        }
                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(LoginActivity.this, R.layout.spinner, langlist);
                        adapter1.setDropDownViewResource(R.layout.spinnerdropdowm);
                        languagespinner.setAdapter(adapter1);
                        languagespinner.setSelection(0);
                    } else {
                        Toast.makeText(getApplicationContext(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        MyApplication.getInstance().addToRequestQueue(strReq);
    }



    // Validating email
    private boolean validateEmail() {
        String email = inputName.getText().toString().trim();
        if (email.isEmpty()) {
            inputName.setError("Enter the Email");
            return false;
        } else {
            inputName.setError(null);
        }

        return true;
    }



    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.eName:
                    validateEmail();
                    break;
                case R.id.ePassword:
                    validateName();
                    break;
            }
        }
    }
}
