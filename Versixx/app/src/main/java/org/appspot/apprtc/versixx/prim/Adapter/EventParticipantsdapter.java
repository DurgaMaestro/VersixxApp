package org.appspot.apprtc.versixx.prim.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.modelclasses.ParticipantssResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


public class EventParticipantsdapter extends RecyclerView.Adapter<EventParticipantsdapter.ViewHolder>
{
    String titlee;
    Context context;
    Vector<ParticipantssResponse> vector;
    String token;
    private String TAG = EventParticipantsdapter.class.getSimpleName();

    public EventParticipantsdapter(Context context, Vector<ParticipantssResponse> vector)
    {
        this.context = context;
         this.vector = new Vector<>();
        this.vector = vector;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {

        View view = LayoutInflater.from(context).inflate(R.layout.participantslist,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {

        holder.tvParticipantname.setText(vector.get(position).getParticipantname());
        holder.tvParticpantEmail.setText(vector.get(position).getParticipantemail());
        holder.tvTitle.setText(vector.get(position).getTitle());
        holder.accepted.setPaintFlags(holder.accepted.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

        if(vector.get(position).getAcceptedstatus().equals("1"))
        {
            holder.accepted.setText("Accepted");
        }

        if(vector.get(position).getAcceptedstatus().equals("2"))
        {
            holder.accepted.setText("Change request");

        }

        if(vector.get(position).getAcceptedstatus().equals("3"))
        {
            holder.accepted.setText("Rejected");
        }
        if(vector.get(position).getAcceptedstatus().equals("4"))
        {
            holder.accepted.setText("Accepted");
        }

        if(vector.get(position).getChangerequestmessage()!=null&&!vector.get(position).getChangerequestmessage().equals(""))
        {
            holder.tvChangerequest.setVisibility(View.VISIBLE);
            holder.tvChangerequest.setText(vector.get(position).getChangerequestmessage());
        }

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAlertDialog(position,vector.get(position).getParticipantid());
            }
        });





    }


    private void callAlertDialog(final int position, final String participantid)
    {

        new AlertDialog.Builder(context)
                .setTitle("Are you sure to delete this participant")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i)
                    {

                        token = MyApplication.getInstance().getPrefManager().getUser().getEmail();

                        callDeleteservice(position,participantid);

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })

                .show();
    }

    private void callDeleteservice(final int position, final String participantid) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.removeeventparticipant, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {

                        new AlertDialog.Builder(context)
                                .setTitle("Alert Message")
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setMessage(obj.getString("msg"))
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialoginterface, int i)
                                    {
                                        vector.remove(position);
                                        notifyItemRemoved(position);
                                        notifyDataSetChanged();
                                    }
                                }).show();



                    } else {
                        Toast.makeText(context, "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(context, "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(context, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                params.put("participantid", participantid);

//                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);


    }


    @Override
    public int getItemCount()
    {
        return vector.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
         TextView tvParticipantname,tvParticpantEmail,tvParticipantstatus,accepted,tvTitle,tvChangerequest;
        ImageView delete;


        public ViewHolder(View itemView)
        {
            super(itemView);

            tvParticipantname = (TextView)itemView.findViewById(R.id.tvParticipantname);
            tvParticpantEmail = (TextView)itemView.findViewById(R.id.tvEmail);
            tvParticipantstatus = (TextView)itemView.findViewById(R.id.tvAccepetedStatus);
            accepted = (TextView)itemView.findViewById(R.id.tvAccepetedStatuss);
            tvTitle = (TextView)itemView.findViewById(R.id.tvParticipanttitle);
            tvChangerequest = (TextView)itemView.findViewById(R.id.tvChangerequest);

            delete = (ImageView)itemView.findViewById(R.id.delete);



        }
    }
}
