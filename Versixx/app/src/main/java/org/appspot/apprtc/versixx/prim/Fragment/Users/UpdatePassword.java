package org.appspot.apprtc.versixx.prim.Fragment.Users;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.MainActivity;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Durga on 08-10-2016.
 */
public class UpdatePassword  extends Fragment {
    EditText name,mobile,cpwd;
    Button send;
    ImageView back;
    private String TAG = UpdatePassword.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.forgetpassword, container, false);
        ((MainActivity) getActivity()).settitle("Change Password");
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.forgetpassword);
        name=(EditText)view.findViewById(R.id.name);
        mobile=(EditText)view.findViewById(R.id.mobile);
        cpwd=(EditText)view.findViewById(R.id.cpwd);
        back = (ImageView)view.findViewById(R.id.img);

        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

//                onBackPressed();

            }
        });
        send=(Button)view.findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(name.getText().toString().equals("")||mobile.getText().toString().equals("")||cpwd.getText().toString().equals(""))
                {

                    Toast.makeText(getActivity().getApplicationContext(), "Enter All Fields", Toast.LENGTH_LONG).show();
                    return;
                }
                else if(!mobile.getText().toString().equals(cpwd.getText().toString()))
                {
                    //Toast.makeText(getApplicationContext(), "Enter All Fields", Toast.LENGTH_LONG).show();

                    //cpass.setError("Password and Confirm Password does not match");
                    Toast.makeText(getActivity().getApplicationContext(), "Password does not match", Toast.LENGTH_LONG).show();
                    cpwd.requestFocus();
                    return;
                }
                else
                {
                    updatePWD();
                }

            }
        });
        return view;
    }
    private void updatePWD() {

        final String namee = name.getText().toString();
        final String mobilee = mobile.getText().toString();
        final String mobcpwdilee = cpwd.getText().toString();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.changepassword, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {

                        Toast.makeText(getActivity().getApplicationContext(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();

                        startActivity(new Intent(getActivity().getApplicationContext(), MainActivity.class));
//                        finish();

                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getActivity().getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getActivity().getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String namse = MyApplication.getInstance().getPrefManager().getUser().getEmail();

                params.put("oldpassword", namee);
                params.put("newpassword", mobilee);
                params.put("confirmpassword", mobcpwdilee);
                params.put("token", namse);


                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

}
