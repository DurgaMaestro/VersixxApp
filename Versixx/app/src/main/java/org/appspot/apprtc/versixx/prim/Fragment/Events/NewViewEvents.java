package org.appspot.apprtc.versixx.prim.Fragment.Events;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.appspot.apprtc.ConnectActivity;
import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.MainActivity;
import org.appspot.apprtc.versixx.prim.Adapter.Commentadapter;
import org.appspot.apprtc.versixx.prim.Adapter.NewViewEventsAdapter;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.modelclasses.CommentListPojo;
import org.appspot.apprtc.versixx.prim.modelclasses.ViewEventModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewViewEvents extends Fragment {
    @Bind(R.id.etLeavecomment)
    EditText etLeavecomment;
    @Bind(R.id.postcomment)
    Button postcomment;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.likebutton)
    ImageView likebutton;
    ImageView videoCall;
    List<CommentListPojo> eventrequestlistList = new ArrayList<>();
    Commentadapter eventadapter;
    LinearLayoutManager layoutManager;
    private String TAG = NewViewEvents.class.getSimpleName();
    int currentPageNumber = 1, totalPages;
    private int MAX_SCROLLING_LIMIT = 10;
    private boolean isAlreadyLoading;
    String token;
    private ArrayList<ViewEventModel> dataList;
    int clear;
    ArrayList<String> participantname;
    ArrayList<String> participantid;
    ArrayList<String> participantemailid;
    ArrayList<String> acceptedstatus;
    ArrayList<String> changerequestmessage;

    TextView itemTitle;
    RecyclerView recycler_view_list;
    TextView tvDate, likes, views, t1, t2, t3;
    TextView tvTitle, tvViews, tvComments, tvUser, tvUserLikes;
    String eventDtailid;
    String videoRoomId = "", videoUserid = "",is_participant="";
    NewViewEventsAdapter itemListDataAdapter;
    TextView cmt;
    public static int CALLID ;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        eventDtailid = getArguments().getString("YourKey");
        View view = inflater.inflate(R.layout.vieweventsdetail, container, false);
        ((MainActivity) getActivity()).settitle("View Event");
        ButterKnife.bind(this, view);
        itemTitle = (TextView) view.findViewById(R.id.itemTitle);
        recycler_view_list = (RecyclerView) view.findViewById(R.id.recycler_view_list);
        tvTitle = (TextView) view.findViewById(R.id.itemTitle);
        cmt = (TextView) view.findViewById(R.id.cmt);
        tvViews = (TextView) view.findViewById(R.id.tvViews);
        tvComments = (TextView) view.findViewById(R.id.tvComments);
        tvUser = (TextView) view.findViewById(R.id.tvUser);
        tvUserLikes = (TextView) view.findViewById(R.id.tvUserLikes);
        videoCall = (ImageView) view.findViewById(R.id.videoCall);
        token = MyApplication.getInstance().getPrefManager().getUser().getEmail();
        dataList = new ArrayList<ViewEventModel>();
        itemListDataAdapter = new NewViewEventsAdapter(getActivity(), dataList);
        recycler_view_list.setHasFixedSize(true);
        recycler_view_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recycler_view_list.setAdapter(itemListDataAdapter);
        recycler_view_list.setNestedScrollingEnabled(false);
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        eventadapter = new Commentadapter(getActivity(), eventrequestlistList, NewViewEvents.this);
        recyclerView.setAdapter(eventadapter);
        participantname = new ArrayList<>();
        participantid = new ArrayList<>();
        participantemailid = new ArrayList<>();
        acceptedstatus = new ArrayList<>();
        changerequestmessage = new ArrayList<>();
        geteventcomments(1);
        if (eventrequestlistList.size() == 0) {
            cmt.setVisibility(View.GONE);
        } else {
            cmt.setVisibility(View.VISIBLE);
        }
        viewevent();
        scrollFunction();
        likebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                posteventlike();
            }
        });
        postcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                posteventcomment();
            }
        });
        videoCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectActivity.RoomID = videoRoomId;
                if (is_participant.equalsIgnoreCase("1")) {
                    //videoRoomId="ram@technotackle.com";
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Would you like make a call on this event as broadcaster?")
                            .setCancelable(false)
                            .setNeutralButton("Production Call", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if (videoRoomId.length() > 0) {
                                        //ConnectActivity.RoomID = "demo@versixx";
                                        EndPoints.TYPE=EndPoints.CASTER;
                                        CALLID = EndPoints.TYPE;
                                        startActivity(new Intent(getActivity(), ConnectActivity.class));
                                    }
                                }
                            })
                            .setPositiveButton("Demo Call", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if (videoRoomId.length() > 0) {
                                        ConnectActivity.RoomID = "demo@versixx";
                                        EndPoints.TYPE=EndPoints.CASTER;
                                        CALLID = EndPoints.TYPE;
                                        startActivity(new Intent(getActivity(), ConnectActivity.class));
                                    }
                                }
                            })
                            .setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else{

                    //videoRoomId="ram@technotackle.com";
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Would you like make a call on this event?")
                            .setCancelable(false)
                            .setNeutralButton("Production Call", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if (videoRoomId.length() > 0) {
                                        //ConnectActivity.RoomID = "demo@versixx";
                                        EndPoints.TYPE=EndPoints.CASTRECEIVER;
                                        CALLID = EndPoints.TYPE;
                                        startActivity(new Intent(getActivity(), ConnectActivity.class));
                                    }
                                }
                            })
                            .setPositiveButton("Demo Call", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    ConnectActivity.RoomID = "demo@versixx";
                                    EndPoints.TYPE=EndPoints.CASTRECEIVER;
                                    CALLID = EndPoints.TYPE;
                                    startActivity(new Intent(getActivity(), ConnectActivity.class));
                                    //startActivity(new Intent(getActivity(), ConnectActivity.class));

                                }
                            })
                            .setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

//                    new AlertDialog.Builder(getActivity())
//                            .setTitle(getText(R.string.invalid_url_title))
//                            .setMessage("Try again Room is busy")
//                            .setCancelable(false)
//                            .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.cancel();
//
//                                }
//                            }).create().show();
                }


            }
        });
        return view;
    }

    private void posteventlike() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.posteventlike, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        likebutton.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getActivity(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                params.put("eventdetailid", eventDtailid);

//                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);


    }

    private void posteventcomment() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.posteventcomment, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        Toast.makeText(getActivity(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                        etLeavecomment.setText("");
                        geteventcomments(1);
                    } else {
                        Toast.makeText(getActivity(), "" + obj.getString("msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("token", token);
                params.put("eventdetailid", eventDtailid);
                params.put("message", etLeavecomment.getText().toString());

//                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);


    }

    private void scrollFunction() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int last_seen_position = layoutManager.findLastVisibleItemPosition();


                if (!isAlreadyLoading
                        && ((last_seen_position + 2) > MAX_SCROLLING_LIMIT)) {

                    isAlreadyLoading = true;
                    // Increase the Scrolling Limit
                    MAX_SCROLLING_LIMIT += 10;

                    if (recyclerView != null) {
                        if (totalPages > currentPageNumber) {
                            currentPageNumber = currentPageNumber + 1;
                            geteventcomments(0);
                        }
                    }
                }
            }
        });
    }

    private void geteventcomments(final int clear) {
        isAlreadyLoading = false;
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.geteventcomments, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {

                        totalPages = obj.getInt("totalpages");
                        JSONArray json_mar = obj.getJSONArray("commentslist");
                        if (clear == 1) {
                            eventrequestlistList.clear();
                        }

                        if (json_mar.length() > 0) {
                            for (int i = 0; i < json_mar.length(); i++) {
                                JSONObject jsob = json_mar.getJSONObject(i);
                                CommentListPojo commentlist = new CommentListPojo(jsob.getString("eventdetailid"), jsob.getString("eventid"),
                                        jsob.getString("commentid"), jsob.getString("commentby"), jsob.getString("message"), jsob.getString("datetime"));
                                eventrequestlistList.add(commentlist);
                                if (eventrequestlistList.size() == 0) {
                                    cmt.setVisibility(View.GONE);
                                } else {
                                    cmt.setVisibility(View.VISIBLE);
                                }
                            }

                        }

                        eventadapter.notifyDataSetChanged();
                        if (eventrequestlistList.size() == 0) {
                            cmt.setVisibility(View.GONE);
                        } else {
                            cmt.setVisibility(View.VISIBLE);
                        }
                    } else if (obj.getInt("status") == 0) {
                        if (eventrequestlistList.size() == 0) {
                            cmt.setVisibility(View.GONE);
                        } else {
                            cmt.setVisibility(View.VISIBLE);
                        }

                    } else if (obj.getInt("status") == -1) {

                        MyApplication.getInstance().logout();
                    }

                } catch (JSONException e) {

                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("token", token);
                params.put("eventdetailid", eventDtailid);
                params.put("pagenumber", String.valueOf(currentPageNumber));
//                Log.e(TAG, "devicetype: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);


    }

    private void viewevent() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.viewevent, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {

                        if (clear == 0) {
                            dataList.clear();
                        }

                        JSONObject eventObj = obj.getJSONObject("eventdetails");


                        try {

                            tvTitle.setText(eventObj.getString("eventtitle"));
                            tvViews.setText("  " + eventObj.getString("viewscount") + "");
                            tvComments.setText(" " + eventObj.getString("commentscount") + " ");
                            tvUser.setText("by " + eventObj.getString("eventcreatedby"));
                            tvUserLikes.setText(" " + eventObj.getString("likescount") + " ");

                            if (eventObj.getString("displaylikebutton").equals("1")) {
                                likebutton.setVisibility(View.VISIBLE);
                            } else {
                                likebutton.setVisibility(View.GONE);
                            }
                            videoUserid = eventObj.getString("video_user_id");
                            videoRoomId = eventObj.getString("video_room_id");
                            is_participant= eventObj.getString("isparticipant");

                            JSONArray participantArray = new JSONArray(eventObj.getString("participants"));
                            ArrayList<ViewEventModel> singleItem = new ArrayList<ViewEventModel>();
                            if (participantArray != null) {
                                for (int j = 0; j < participantArray.length(); j++) {
                                    String Item = participantArray.getJSONObject(j).getString("participantname");
                                    String Item1 = participantArray.getJSONObject(j).getString("participantid");
                                    String Item2 = participantArray.getJSONObject(j).getString("participantemailid");
                                    String Item3 = participantArray.getJSONObject(j).getString("participantprofileimage");
                                    String Item4 = participantArray.getJSONObject(j).getString("votescount");
                                    String Item5 = participantArray.getJSONObject(j).getString("displayvotebutton");
                                    String Item6 = participantArray.getJSONObject(j).getString("displayrevertbutton");

                                    dataList.add(new ViewEventModel(Item1, Item, Item2, Item3, eventObj.getString("eventdetailid"), Item4, Item5, Item6));
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        itemListDataAdapter.notifyDataSetChanged();
                    } else if (obj.getInt("status") == 0) {

                    } else if (obj.getInt("status") == -1) {
                        MyApplication.getInstance().logout();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String name = MyApplication.getInstance().getPrefManager().getUser().getEmail();
                params.put("eventdetailid", eventDtailid);
                params.put("token", name);
                params.put("pagenumber", currentPageNumber + "");
//                Log.e(TAG, "devicetype: " + name + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }


}
