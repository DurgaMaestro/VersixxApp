package org.appspot.apprtc.versixx.prim.Fragment.HomePage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.wang.avi.AVLoadingIndicatorView;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Adapter.HomeCompletedEventAdapter;
import org.appspot.apprtc.versixx.prim.Fragment.Events.NewViewEvents;
import org.appspot.apprtc.versixx.prim.app.ClickListener;
import org.appspot.apprtc.versixx.prim.app.EndPoints;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.app.RecyclerTouchListener;
import org.appspot.apprtc.versixx.prim.modelclasses.EventsDataModel;
import org.appspot.apprtc.versixx.prim.modelclasses.ParticipantsItemModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


public class Completed extends Fragment {


    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.loading_bar)
    AVLoadingIndicatorView loading;
    @Bind(R.id.tvErrorView)
    TextView tvErrorView;
    int clear = 0;
    LinearLayoutManager layoutManager;
    private String TAG = Completed.class.getSimpleName();
    int currentPageNumber = 1, totalPages;
    private int MAX_SCROLLING_LIMIT = 10;
    private boolean isAlreadyLoading;
    String token;
    ArrayList<EventsDataModel> allSampleData;
    ArrayList<String> participantname;
    ArrayList<String> participantid;
    ArrayList<String> participantemailid;
    ArrayList<String> acceptedstatus;
    ArrayList<String> changerequestmessage;
    HomeCompletedEventAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.genericrecyclerview1, container, false);
        ButterKnife.bind(this, view);
        token = MyApplication.getInstance().getPrefManager().getUser().getEmail();
        allSampleData = new ArrayList<EventsDataModel>();
        participantname = new ArrayList<>();
        participantid = new ArrayList<>();
        participantemailid = new ArrayList<>();
        acceptedstatus = new ArrayList<>();
        changerequestmessage = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new HomeCompletedEventAdapter(getActivity(), allSampleData);
        recyclerView.setAdapter(adapter);
        loading.setVisibility(View.GONE);
        scrollFunction();
        currentPageNumber = 1;
        MAX_SCROLLING_LIMIT=10;
        isAlreadyLoading = false;
        myevents(1);


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),
                recyclerView, new ClickListener() {

            @Override
            public void onClick(View view, final int position) {

                NewViewEvents fragment = new NewViewEvents();
                Bundle args = new Bundle();
                args.putString("YourKey", allSampleData.get(position).getEventdetailid());
                fragment.setArguments(args);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();

            }

            @Override
            public void onLongClick(View view, int position) {


            }

        }));
        return view;
    }

    private void scrollFunction() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int last_seen_position = layoutManager.findLastVisibleItemPosition();
                Log.e(TAG, "last_seen_position: "+last_seen_position + currentPageNumber+MAX_SCROLLING_LIMIT+isAlreadyLoading);
                if (!isAlreadyLoading
                        && ((last_seen_position + 2) > MAX_SCROLLING_LIMIT)) {
                    Log.e(TAG, "last_seen_posifffffffffftion: "+last_seen_position + currentPageNumber);
                    isAlreadyLoading = true;
                    MAX_SCROLLING_LIMIT += 10;

                    if (recyclerView != null) {
                        if (totalPages > currentPageNumber) {
                            currentPageNumber = currentPageNumber + 1;
                            Log.e(TAG, "currentPageNumbers: "+totalPages + currentPageNumber);
                            loading.setVisibility(View.VISIBLE);
                            clear = 1;
                            myevents(0);
                        }
                    }
                }
            }
        });
    }

    private void myevents(final int clear) {
        isAlreadyLoading = false;
        loading.setVisibility(View.GONE);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.listallcompletedevents, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("status") == 1) {
                        totalPages = obj.getInt("totalpages");
                        currentPageNumber=obj.getInt("currentpageno");
                        Log.e(TAG, "currentPageNumber: "+totalPages + currentPageNumber);
                        if (clear == 1) {
                            allSampleData.clear();
                        }

                        JSONArray eventArray = obj.getJSONArray("eventslist");
                        for (int i = 0; i < eventArray.length(); i++) {
                            EventsDataModel dm = new EventsDataModel();
                            JSONObject objevents = (JSONObject) eventArray.get(i);
                            try {
                                dm.setHeaderTitle("" + objevents.getString("eventtitle"));
                                dm.setEventdetailid(objevents.getString("eventdetailid"));
                                dm.setEventid(objevents.getString("eventid"));
                                dm.setEventtitle(objevents.getString("eventtitle"));
                                dm.setEventdate(objevents.getString("eventdate"));
                                dm.setEventduration(objevents.getString("eventduration"));
                                dm.setparticipantstype(objevents.getString("participantstype"));
                                dm.setnoofparticipants(objevents.getString("noofparticipants"));
                                dm.setviewscount(objevents.getString("viewscount"));
                                dm.setlikescount(objevents.getString("likescount"));
                                JSONArray ParticipantArray = new JSONArray(eventArray.getJSONObject(i).getString("participants"));
                                ArrayList<ParticipantsItemModel> singleItem = new ArrayList<ParticipantsItemModel>();
                                if (ParticipantArray != null) {
                                    for (int j = 0; j < ParticipantArray.length(); j++) {
                                        String Item = ParticipantArray.getJSONObject(j).getString("participantname");
                                        String Item1 = ParticipantArray.getJSONObject(j).getString("participantid");
                                        String Item2 = ParticipantArray.getJSONObject(j).getString("participantemailid");
                                        String Item3 = ParticipantArray.getJSONObject(j).getString("participantprofileimage");
                                        singleItem.add(new ParticipantsItemModel(Item1, Item, Item2, Item3, objevents.getString("eventdetailid")));
                                    }
                                }
                                dm.setAllItemsInSection(singleItem);

                                allSampleData.add(dm);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                        adapter.notifyDataSetChanged();
                    } else if (obj.getInt("status") == 0) {
                        tvErrorView.setVisibility(View.VISIBLE);
                    } else if (obj.getInt("status") == -1) {
                        MyApplication.getInstance().logout();
                    }

                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String name = MyApplication.getInstance().getPrefManager().getUser().getEmail();
                params.put("searchkey", "");
                params.put("token", name);
                params.put("pagenumber", currentPageNumber + "");
//                Log.e(TAG, "devicetype: " + name + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }


}
