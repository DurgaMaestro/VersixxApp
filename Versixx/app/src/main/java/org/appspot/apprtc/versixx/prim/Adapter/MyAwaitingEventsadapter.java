package org.appspot.apprtc.versixx.prim.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.app.MyApplication;
import org.appspot.apprtc.versixx.prim.modelclasses.AwaitingEventsResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class MyAwaitingEventsadapter extends RecyclerView.Adapter<MyAwaitingEventsadapter.ViewHolder> {
    String titlee;
    Context context;
    List<AwaitingEventsResponse> eventslist;
    String token;
    String eventdetailid, eventid;
    int priorityy = 0;
    String alertMessage;
    int notifypostion;

    public MyAwaitingEventsadapter(Context context, List<AwaitingEventsResponse> eventslist) {
        this.context = context;
        this.eventslist = eventslist;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.awaitingeventlist, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        token = MyApplication.getInstance().getPrefManager().getUser().getEmail();
        titlee = eventslist.get(position).getEventtitle();
        holder.tvTitle.setText(eventslist.get(position).getEventtitle());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
        String dtStart = eventslist.get(position).getEventdate();
        try {
            Date d = format.parse(dtStart);
            format.format(d.getTime()+ TimeZone.getDefault().getRawOffset());

            System.out.println(format.format(d.getTime() + TimeZone.getDefault().getRawOffset()) + " DateTime(date);"+eventslist.get(position).getEventdate());
            holder.duration.setText(format.format(d.getTime() + TimeZone.getDefault().getRawOffset()));

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        holder.no_participants.setText("No of participants : " + eventslist.get(position).getnoofparticipants());
        holder.participantType.setText("Participants type : " + eventslist.get(position).getparticipantstype());

        if (eventslist.get(position).getparticipantstype().equals("Open")) {
            holder.go.setVisibility(View.GONE);
        }
        String powers = "";
        List h = eventslist.get(position).getChatlist();

        holder.no_participantss.setText(powers);



//        String dtStart = "2016-11-11 06:30 PM";
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
//        try {
//            Date d = format.parse(dtStart);
//            format.format(d.getTime()+TimeZone.getDefault().getRawOffset());
//
//            System.out.println(format.format(d.getTime()+TimeZone.getDefault().getRawOffset())+" DateTime(date);");
//        } catch (ParseException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }

    }




    @Override
    public int getItemCount() {
        return eventslist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView duration, tvTitle, no_participants, participantType, no_participantss;
        ImageView go;
        LinearLayout particpantlayout, particpantlayout1, lay3;


        public ViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.profileimage);
            duration = (TextView) itemView.findViewById(R.id.tvDuration);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            no_participants = (TextView) itemView.findViewById(R.id.noofparticipant);
            no_participantss = (TextView) itemView.findViewById(R.id.noofparticipants);
            participantType = (TextView) itemView.findViewById(R.id.participanttype);
            go = (ImageView) itemView.findViewById(R.id.accept);

        }
    }
}
