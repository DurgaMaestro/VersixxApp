package org.appspot.apprtc.multipeer;

import org.webrtc.IceCandidate;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.SessionDescription;
import org.webrtc.StatsReport;

/**
 * Peer connection events.
 */
public interface PeerConnectionEvents {

    public void sendOffer(final SessionDescription sdp, final BasePeerConnection peerConnection);

    public void sendAnswer(final SessionDescription sdp, final BasePeerConnection peerConnection);

    /**
     * Callback fired once local SDP is created and set.
     */
//    void onLocalDescription(final SessionDescription sdp, BasePeerConnection peerConnection);

    /**
     * Callback fired once local Ice candidate is generated.
     */
    void onIceCandidate(final IceCandidate candidate, BasePeerConnection peerConnection);

    /**
     * Callback fired once local ICE candidates are removed.
     */
    void onIceCandidatesRemoved(final IceCandidate[] candidates, BasePeerConnection peerConnection);

    /**
     * Callback fired once connection is established (IceConnectionState is
     * CONNECTED).
     */
    void onIceConnected();

    /**
     * Callback fired once connection is closed (IceConnectionState is
     * DISCONNECTED).
     */
    void onIceDisconnected();

    /**
     * Callback fired once peer connection is closed.
     */
    void onPeerConnectionClosed();

    /**
     * Callback fired once peer connection statistics is ready.
     */
    void onPeerConnectionStatsReady(final StatsReport[] reports);

    /**
     * Callback fired once peer connection error happened.
     */
    void onPeerConnectionError(final String description);

    void onAddStream(MediaStream stream, String broadcastId, PeerConnection peerConnection);

    void closeConnection(String broadcastId);
}
