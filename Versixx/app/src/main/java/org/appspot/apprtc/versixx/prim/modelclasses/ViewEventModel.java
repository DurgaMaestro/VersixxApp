package org.appspot.apprtc.versixx.prim.modelclasses;


public class ViewEventModel {

    public ViewEventModel(String participantid, String participantname, String participantemailid, String participantprofileimage, String eventid, String votescount, String displayvotebutton, String displayrevertbutton)
    {
        this.participantid = participantid;
        this.participantname = participantname;
        this.participantemailid = participantemailid;
        this.participantprofileimage  =participantprofileimage ;
        this.eventid = eventid;
        this.votescount = votescount;
        this.displayvotebutton = displayvotebutton;
        this.displayrevertbutton = displayrevertbutton;


    }
    private boolean selected=false;
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    private String votescount;

    private String displayvotebutton;

    private String displayrevertbutton;

    private String eventid;

    private String participantid;

    private String participantname;

    private String participantemailid;

    private String video_room_id;

    private String video_user_id ;

    private String participantprofileimage ;

    public String geteventid() {
        return eventid;
    }

    public void seteventid(String eventid) {
        this.eventid = eventid;
    }

    public String getparticipantid() {
        return participantid;
    }

    public void setparticipantid(String participantid) {
        this.participantid = participantid;
    }
    public String getVotescount() {
        return votescount;
    }

    public void setVotescount(String votescount) {
        this.votescount = votescount;
    }

    public String getDisplayvotebutton() {
        return displayvotebutton;
    }

    public String getVideo_room_id() {
        return video_room_id;
    }

    public void setVideo_room_id(String video_room_id) {
        this.video_room_id = video_room_id;
    }

    public String getVideo_user_id() {
        return video_user_id;
    }

    public void setVideo_user_id(String video_user_id) {
        this.video_user_id = video_user_id;
    }

    public void setDisplayvotebutton(String displayvotebutton) {
        this.displayvotebutton = displayvotebutton;

    }

    public String getDisplayrevertbutton() {
        return displayrevertbutton;
    }

    public void setDisplayrevertbutton(String displayrevertbutton) {
        this.displayrevertbutton = displayrevertbutton;
    }
    public String getparticipantname() {
        return participantname;
    }

    public void setparticipantname(String participantname) {
        this.participantname = participantname;
    }

    public String getparticipantemailid() {
        return participantemailid;
    }

    public void setparticipantemailid(String participantemailid) {
        this.participantemailid = participantemailid;
    }

    public String getparticipantprofileimage() {
        return participantprofileimage;
    }

    public void setparticipantprofileimage(String participantprofileimage) {
        this.participantprofileimage = participantprofileimage;
    }

}
