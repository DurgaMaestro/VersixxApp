package org.appspot.apprtc.versixx.prim.app;


public class EndPoints {
//    public static final String BASE_URL = "http://maestro.com/versix/webservice/";
//    public static final String IMAGE_BASE_URL = "http://maestro.com/versix/";
    public static final String BASE_URL = " http://versixx.com/demo/webservice/";
    public static final String IMAGE_BASE_URL = "http://versixx.com/demo";
    public static final String LOGOUT = BASE_URL + "logout";
    public static final String LOGIN = BASE_URL + "login";
    public static final String SignUp = BASE_URL + "signup";
    public static final String getmyprofile = BASE_URL + "getmyprofile";
    public static final String checkdevice = BASE_URL + "checkdevice";
    public static final String updatemyprofile = BASE_URL + "updatemyprofile";
    public static final String changepassword = BASE_URL + "changepassword";
    public static final String forgotpassword = BASE_URL + "forgotpassword";
    public static final String listalleventsubcategory = BASE_URL + "listalleventsubcategory";
    public static final String searchuser = BASE_URL + "searchuser";
    public static final String createevent = BASE_URL + "createevent";
    public static final String eventparticipaterequests = BASE_URL + "eventparticipaterequests";
    public static final String accepteventparticipaterequest = BASE_URL + "accepteventparticipaterequest";
    public static final String rejecteventparticipaterequest = BASE_URL + "rejecteventparticipaterequest";
    public static final String listalleventcategory = BASE_URL + "listalleventcategory";
    public static final String changeeventparticipaterequest = BASE_URL + "changeeventparticipaterequest";
    public static final String availableopenevents = BASE_URL + "availableopenevents";
    public static final String acceptopenevent = BASE_URL + "acceptopenevent";
    public static final String myawaitingevents = BASE_URL + "myawaitingevents";
    public static final String removeeventparticipant = BASE_URL + "removeeventparticipant";
    public static final String listallmyfriends = BASE_URL + "listallmyfriends";
    public static final String friendrequests = BASE_URL + "friendrequests";
    public static final String acceptfriendrequest = BASE_URL + "acceptfriendrequest";
    public static final String sendfriendrequest = BASE_URL + "sendfriendrequest";
    public static final String friendssuggestion = BASE_URL + "friendssuggestion";
    public static final String rejectfriendrequest = BASE_URL + "rejectfriendrequest";
    public static final String unfriend = BASE_URL + "unfriend";
    public static final String reverteventvote = BASE_URL + "reverteventvote";
    public static final String posteventvote = BASE_URL + "posteventvote";
    public static final String posteventlike = BASE_URL + "posteventlike";
    public static final String listallcompletedevents = BASE_URL + "listallcompletedevents";
    public static final String listallongoingevents = BASE_URL + "listallongoingevents";
    public static final String listallupcomingevents = BASE_URL + "listallupcomingevents";
    public static final String viewevent = BASE_URL + "viewevent";
    public static final String geteventcomments = BASE_URL + "geteventcomments";
    public static final String posteventcomment = BASE_URL + "posteventcomment";
    public static final String listallcountry = BASE_URL + "listallcountry";
    public static final String lang = BASE_URL + "listalllanguage";


    public static int TYPE;
    public static final int CASTER=1;
    public static final int CASTRECEIVER=2;
    public static final int CASTVIWER=3;

}
