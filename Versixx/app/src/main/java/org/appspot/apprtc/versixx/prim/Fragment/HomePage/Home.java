package org.appspot.apprtc.versixx.prim.Fragment.HomePage;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.appspot.apprtc.R;
import org.appspot.apprtc.versixx.prim.Activities.MainActivity;

import butterknife.Bind;
import butterknife.ButterKnife;


public class Home extends Fragment
{
    @Bind(R.id.pager)
    ViewPager viewPager;
    @Bind(R.id.tab_layout)
    TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.askbattle,container,false);
        ((MainActivity) getActivity()).settitle("Home");
        ButterKnife.bind(this,view);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }



    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity(),getChildFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);

    }

    public static class ViewPagerAdapter extends FragmentPagerAdapter {
        String[] mFragmentTitleList = {"   COMPLETED   ","   ON GOING   ", "   UP COMING   "};

        Context context;
        public ViewPagerAdapter(FragmentActivity activity, FragmentManager fm) {
            super(fm);
            this.context = activity;
        }

        @Override
        public int getCount() {
            return mFragmentTitleList.length;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                Completed tab1 = new Completed();
                return tab1;
            }
            else if(position==1)
            {
                Ongoing tab2 = new Ongoing();
                return tab2;
            }
            else
            {
                Upcoming tab3 = new Upcoming();
                return tab3;
            }


        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList[position];
        }
    }

}
