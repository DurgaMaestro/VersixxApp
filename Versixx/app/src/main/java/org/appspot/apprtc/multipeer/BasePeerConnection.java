package org.appspot.apprtc.multipeer;

import org.webrtc.IceCandidate;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;

import java.lang.ref.WeakReference;
import java.util.LinkedList;

/**
 */

public class BasePeerConnection  {
    public boolean isInitiator;
    public PeerConnection peerConnection;
    public PeerConnectionClient.SDPObserver sdpObserver;
    public PeerConnectionClient.PCObserver pcObserver;
    public String broadcastId;
    private WeakReference<MediaStream> addedStream;
    // Queued remote ICE candidates are consumed only after both local and
    // remote descriptions are set. Similarly local ICE candidates are sent to
    // remote peer after both local and remote description are set.
    public LinkedList<IceCandidate> queuedRemoteCandidates;

    public BasePeerConnection(PeerConnection peerConnection, PeerConnectionClient.SDPObserver sdpObserver, PeerConnectionClient.PCObserver pcObserver, String broadcastId) {
        this.peerConnection = peerConnection;
        this.sdpObserver = sdpObserver;
        this.pcObserver = pcObserver;
        this.broadcastId = broadcastId;
        queuedRemoteCandidates = new LinkedList<>();
    }

    public void addStream(MediaStream mediaStream) {
        addedStream = new WeakReference<>(mediaStream);
        peerConnection.addStream(mediaStream);
    }

    public void removeStream() {
        try {
            MediaStream mediaStream = addedStream.get();
            if(mediaStream != null && peerConnection != null) {
                peerConnection.removeStream(mediaStream);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
